package com.example.mynamebattler.ManagerPackage;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PlayerPackage.Job.Cleric;
import com.example.mynamebattler.PlayerPackage.Job.Fighter;
import com.example.mynamebattler.PlayerPackage.Job.Hunter;
import com.example.mynamebattler.PlayerPackage.Job.Knight;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.PlayerPackage.Job.Warrior;
import com.example.mynamebattler.PlayerPackage.Job.Wizard;
import com.example.mynamebattler.PlayerPackage.Job.Cheater;

import java.util.ArrayList;
/*
* キャラクターデータを操作するクラス
 */
public class CharacterDataManager implements Constant {

    private DBManager dbManager;

    private static final CharacterDataManager INSTANCE = new CharacterDataManager();

    private CharacterDataManager(){
        dbManager = DBManager.getInstance();
    }

    //シングルトンパターン
    public static CharacterDataManager getInstance(){
        return INSTANCE;
    }

    //選択された職業IDに応じて作成する職業を分岐させ、新規にキャラクターデータを作成する
    public PlayerData CharacterDataCreate(String characterName , int jobID){

        switch (jobID){
            case FIGHTER_ID:
                PlayerData fighter = new Fighter(characterName);
                return fighter;
            case WIZARD_ID:
                PlayerData wizard = new Wizard(characterName);
                return wizard;
            case CLERIC_ID:
                PlayerData cleric = new Cleric(characterName);
                return cleric;
            case KNIGHT_ID:
                PlayerData knight = new Knight(characterName);
                return knight;
            case HUNTER_ID:
                PlayerData hunter = new Hunter(characterName);
                return hunter;
            case WARRIOR_ID:
                PlayerData warrior = new Warrior(characterName);
                return warrior;
            case CHEATER_ID:
                PlayerData cheater = new Cheater(characterName);
                return cheater;
            default:
                return null;
        }
    }

    //パーティー編成するキャラクターの情報を取得する
    public PlayerData GetCharacterData(int characterId, int jobId, boolean myCharacter){

        String dummyName = "Dummy"; //ダミーのキャラクター名

        //職業名から職業を判定し、DBに保存されたステータスを反映する
        switch (jobId){
            case FIGHTER_ID:
                PlayerData fighter = new Fighter(dummyName);
                fighter = SetStatus(fighter,characterId,myCharacter);
                return fighter;
            case WIZARD_ID:
                PlayerData wizard = new Wizard(dummyName);
                wizard = SetStatus(wizard,characterId,myCharacter);
                return wizard;
            case CLERIC_ID:
                PlayerData cleric = new Cleric(dummyName);
                cleric = SetStatus(cleric,characterId,myCharacter);
                return cleric;
            case KNIGHT_ID:
                PlayerData knight = new Knight(dummyName);
                knight = SetStatus(knight,characterId,myCharacter);
                return knight;
            case HUNTER_ID:
                PlayerData hunter = new Hunter(dummyName);
                hunter = SetStatus(hunter,characterId,myCharacter);
                return hunter;
            case WARRIOR_ID:
                PlayerData warrior = new Warrior(dummyName);
                warrior = SetStatus(warrior,characterId,myCharacter);
                return warrior;
            case CHEATER_ID:
                PlayerData cheater = new Cheater(dummyName);
                cheater = SetStatus(cheater,characterId,myCharacter);
                return cheater;
            default:
                return null;
        }
    }

    //DBに保存されたステータスをセット
    private PlayerData SetStatus(PlayerData playerData, int characterId, boolean myCharacter){
        //myCharacterがtrueなら自キャラ、falseならエネミー側のデータを取得
        if(myCharacter){
            ArrayList<String> myCharaInfo = dbManager.GetCharacterInfo(characterId);
            playerData.setCharaId(Integer.valueOf(myCharaInfo.get(0)));
            playerData.setPlayerName(myCharaInfo.get(1));
            playerData.setJobId(Integer.valueOf(myCharaInfo.get(2)));
            playerData.setDefaultHp(Integer.valueOf(myCharaInfo.get(3)));
            playerData.setDefaultStr(Integer.valueOf(myCharaInfo.get(4)));
            playerData.setDefaultDef(Integer.valueOf(myCharaInfo.get(5)));
            playerData.setDefaultAgi(Integer.valueOf(myCharaInfo.get(6)));
            playerData.setDefaultDex(Integer.valueOf(myCharaInfo.get(7)));
            playerData.setDefaultLuck(Integer.valueOf(myCharaInfo.get(8)));
            playerData.setPaId(Integer.valueOf(myCharaInfo.get(9)));
            playerData.setSkillList(dbManager.GetAllSkill(dbManager.GetJobInfo(playerData.getJobId()).get(7)));
            playerData.StateReset();
            playerData.setEnemyFrag(false);
        } else {
            ArrayList<String> enemyInfo = dbManager.GetEnemyInfo(characterId);
            playerData.setCharaId(Integer.valueOf(enemyInfo.get(0)));
            playerData.setPlayerName(enemyInfo.get(1));
            playerData.setJobId(Integer.valueOf(enemyInfo.get(2)));
            playerData.setDefaultHp(Integer.valueOf(enemyInfo.get(3)));
            playerData.setDefaultStr(Integer.valueOf(enemyInfo.get(4)));
            playerData.setDefaultDef(Integer.valueOf(enemyInfo.get(5)));
            playerData.setDefaultAgi(Integer.valueOf(enemyInfo.get(6)));
            playerData.setDefaultDex(Integer.valueOf(enemyInfo.get(7)));
            playerData.setDefaultLuck(Integer.valueOf(enemyInfo.get(8)));
            playerData.setPaId(Integer.valueOf(enemyInfo.get(9)));
            playerData.setSkillList(dbManager.GetAllSkill(dbManager.GetJobInfo(playerData.getJobId()).get(7)));
            playerData.StateReset();
            playerData.setEnemyFrag(true);
        }

        return playerData;
    }

    //キャラクター名からキャラクター情報を取得する
    public int GetCharacterID(String characterName){
        return dbManager.GetCharacterID(characterName);
    }

    //指定したキャラクター情報を取り出す
    public ArrayList<String> GetCharacterInfo(int characterId){
        return dbManager.GetCharacterInfo(characterId);
    }

    //キャラクター情報をDBに保存する
    public void InsertCharacterData(String chara_name,
                                    int job_id, int hp, int str, int def, int agi, int dex, int luck,
                                    int pa_id){
        dbManager.InsertCharacterData(chara_name,job_id,hp,str,def,agi,dex,luck,pa_id);
    }

    //DBに既に登録されている名前かチェック(true:重複なし/false:重複あり)
    public boolean CharacterNameDuplicationCheck(String characterName){
        if(dbManager.GetCharacterID(characterName) == 0) {
            return true;
        } else {
            return false;
        }
    }

    //DBから指定したキャラクターを削除する
    public void DeleteCharacterData(int characterId){
        dbManager = DBManager.getInstance();
        dbManager.DeleteCharacterData(characterId);
    }

    //保存された全てのキャラのキャラクターIDを取得
    public ArrayList<Integer> GetAllCharacterID(){
        return dbManager.GetAllCharacterID();
    }

    //保存された全てのキャラのキャラクター名を取得
    public ArrayList<String> GetAllCharacterName(){
        return dbManager.GetAllCharacterName();
    }

    //保存された全てのキャラのジョブ名を取得
    public ArrayList<String> GetAllCharacterJob(){
        return dbManager.GetAllCharacterJob();
    }

    //保存された全てのキャラのPAを取得
    public ArrayList<String> GetAllCharacterPA(){
        return dbManager.GetAllCharacterPA();
    }
}
