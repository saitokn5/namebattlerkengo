package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　PA：204　底力
  【発動条件】自身のHP：20％未満
  自身のSTR＆DEF＆AGL＆DEXを20％アップする
 */
public class PA204_CRISIS_POWER implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {
        double targetHp = actionMember.getDefaultHp() * 0.2;
        //発動条件判定
        if(actionMember.getBattleHp() < (int)targetHp){
            //PA未発動の場合
            if(!actionMember.isPaCheck()){
                actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n瀕死の状況に闘志が燃え上がる！\n\n");
                actionMember.setBattleStr(actionMember.getBattleStr() + (int)(actionMember.getDefaultStr() * 0.2));
                actionMember.setBattleDef(actionMember.getBattleDef() + (int)(actionMember.getDefaultDef() * 0.2));
                actionMember.setBattleAgi(actionMember.getBattleAgi() + (int)(actionMember.getDefaultAgi() * 0.2));
                actionMember.setBattleDex(actionMember.getBattleDex() + (int)(actionMember.getDefaultDex() * 0.2));
                actionMember.setPaCheck(true);
            }
        } else {
            //HP20％以上の場合
            //PA発動中の場合
            //ステータスの加算値を取る
            if(actionMember.isPaCheck()){
                actionMember.setBattleStr(actionMember.getBattleStr() - (int)(actionMember.getDefaultStr() * 0.2));
                actionMember.setBattleDef(actionMember.getBattleDef() - (int)(actionMember.getDefaultDef() * 0.2));
                actionMember.setBattleAgi(actionMember.getBattleAgi() - (int)(actionMember.getDefaultAgi() * 0.2));
                actionMember.setBattleDex(actionMember.getBattleDex() - (int)(actionMember.getDefaultDex() * 0.2));
                actionMember.setPaCheck(false);
            }
        }
    }
}
