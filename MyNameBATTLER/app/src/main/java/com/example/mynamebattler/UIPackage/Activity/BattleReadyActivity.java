package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.PartyPackage.MyParty;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Fragment.EnemyPartyFragment;
import com.example.mynamebattler.UIPackage.Fragment.HeaderFragment;
import com.example.mynamebattler.UIPackage.Fragment.MyPartyFragment;
/*
　バトル準備画面の表示及びUI処理を行うクラス
 */
public class BattleReadyActivity extends AppCompatActivity implements Constant {

    private PartyManager partyManager;
    private Party myParty;
    private Bundle bundle;
    private MyPartyFragment myPartyFragment;
    private EnemyPartyFragment enemyPartyFragment;
    private HeaderFragment headerFragment;
    private ImageManager imageManager;
    private SoundManager soundManager;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battle_ready_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスを取得
        partyManager = PartyManager.getInstance();
        imageManager = ImageManager.getInstance();
        soundManager = SoundManager.getInstance();

        //EnemyPartyを作成
        partyManager.EnemyPartyCreate();

        InitHeaderFragment();
        InitMyPartyFragment();
        InitEnemyPartyFragment();
        InitVSImage();
        InitButtonUI();
    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //HeaderFragmentの表示
    private void InitHeaderFragment(){

        bundle.putString(BACK_ACT_CHECK_KEY,BACK_PARTY_FORMATION_ACT_KEY);
        headerFragment = new HeaderFragment();
        headerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.headerFragmentFrame,headerFragment);
        transaction.commit();
    }

    //MyPartyFragmentの設定
    private void InitMyPartyFragment(){
        //パーティー情報を取得
        myParty = new MyParty(partyManager.getMyParty());

        //Bundleへパーティー情報を格納
        bundle.putSerializable(MYPARTY_PLAYER1_KEY,myParty.getParty().get(0));
        bundle.putSerializable(MYPARTY_PLAYER2_KEY,myParty.getParty().get(1));
        bundle.putSerializable(MYPARTY_PLAYER3_KEY,myParty.getParty().get(2));

        //MyPartyFragmentにパーティーを送り、更新
        myPartyFragment = new MyPartyFragment();
        myPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mypartyFlame,myPartyFragment);
        transaction.commit();

    }
    
    //EnemyPartyFragmentの設定
    private void InitEnemyPartyFragment(){

        enemyPartyFragment = new EnemyPartyFragment();

        //Bundleへパーティー情報を格納
        bundle.putSerializable(ENEMYPARTY_PLAYER1_KEY,partyManager.getEnemyParty().get(0));
        bundle.putSerializable(ENEMYPARTY_PLAYER2_KEY,partyManager.getEnemyParty().get(1));
        bundle.putSerializable(ENEMYPARTY_PLAYER3_KEY,partyManager.getEnemyParty().get(2));

        //MyPartyFragmentにパーティーを送り、更新
        enemyPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.enemypartyFlame,enemyPartyFragment);
        transaction.commit();

    }

    //VS画像の表示
    private void InitVSImage(){
        ImageView VSImageView = findViewById(R.id.VSImageView);

        Bitmap image = imageManager.GetImageBitmap(this,VS_IMAGE_FILENAME);
        VSImageView.setImageBitmap(image);
    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //バトル画面へ遷移
        ImageButton sendBattleSceneActBtn = findViewById(R.id.SendBattleSceneActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.BATTLE_START_BUTTON_IMAGE);
        sendBattleSceneActBtn.setImageBitmap(btnImage2);
        sendBattleSceneActBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),BattleSceneActivity.class);
                startActivity(intent);
            }
        });

        //EnemyPartyを更新
        ImageButton enemyUpdateBtn = findViewById(R.id.EnemyUpdataButton);
        Bitmap btnImage3 = imageManager.GetImageBitmap(this,ENEMY_UPDATE_BUTTON_IMAGE);
        enemyUpdateBtn.setImageBitmap(btnImage3);
        enemyUpdateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                partyManager.EnemyPartyUpdate();
                InitEnemyPartyFragment();
            }
        });
    }
}
