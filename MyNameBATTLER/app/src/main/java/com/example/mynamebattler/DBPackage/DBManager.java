package com.example.mynamebattler.DBPackage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.SkillPackage.Skill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
/*
　データベース操作を管理するクラス
 */
public class DBManager implements Constant {

    private static final DBManager INSTANCE = new DBManager();
    private DBOpenHelper helper;
    private SQLiteDatabase db;

    //シングルトンパターン
    private DBManager(){
    }

    public static DBManager getInstance(){
        return INSTANCE;
    }

    //データベースを作成する
    public void CreateDB(Context context){
        if(helper == null){
            helper = new DBOpenHelper(context);
        }
        if(db == null){
            db = helper.getWritableDatabase();
        }
    }

    //ジョブスピナー設定用の職業名一覧をDBから取得する
   public ArrayAdapter<String> GetJobSpinnerItem(Context context){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_JOB_NAME + " FROM " + JOB_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                arrayAdapter.add(cursor.getString(0));

                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
       return arrayAdapter;

   }

    //PAスピナー設定用の職業名一覧をDBから取得する
    public ArrayAdapter<String> GetPASpinnerItem(Context context){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_PA_NAME + " FROM " + PA_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                arrayAdapter.add(cursor.getString(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return arrayAdapter;

    }

    //キャラクター名からキャラクターIDを取得
    public int GetCharacterID(String characterName){
        int characterId;

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + CHARACTER_TABLE +
                            " WHERE " + COLUMN_CHARA_NAME + " ='" + characterName + "';",
                    null
            );

            cursor.moveToFirst();

            characterId = cursor.getInt(0);

            cursor.close();
        } catch (Exception e){
            characterId = 0;
            e.printStackTrace();
        }

        return characterId;
    }

    //職業名から職業IDを取得
    public int GetJobID(String JobName){
        int jobID;

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + JOB_TABLE +
                            " WHERE " + COLUMN_JOB_NAME + " ='" + JobName + "';",
                    null
            );

            cursor.moveToFirst();

            jobID = cursor.getInt(0);

            cursor.close();
        } catch (Exception e){
            jobID = 0;
            e.printStackTrace();
        }

        return jobID;
    }

    //PA名からPAIDを取得
    public int GetPAID(String PAName){
        int paID;

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + PA_TABLE +
                            " WHERE " + COLUMN_PA_NAME + " ='" + PAName + "';",
                    null
            );

            cursor.moveToFirst();

            paID = cursor.getInt(0);

            cursor.close();
        } catch (Exception e){
            paID = 0;
            e.printStackTrace();
        }
        return paID;
    }

    //JOB_IDから職業情報を取得
    public ArrayList<String> GetJobInfo(int jobID){

        ArrayList<String> jobInfo = new ArrayList<String>();
        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + JOB_TABLE +
                            " WHERE " + COLUMN_JOB_ID + " ='" + jobID + "';",
                    null
            );

            cursor.moveToFirst();

            jobInfo.add(cursor.getString(1)); //JOB_NAME
            jobInfo.add(cursor.getString(2)); //HP_RANGE
            jobInfo.add(cursor.getString(3)); //STR_RANGE
            jobInfo.add(cursor.getString(4)); //DEF_RANGE
            jobInfo.add(cursor.getString(5)); //AGI_RANGE
            jobInfo.add(cursor.getString(6)); //DEX_RANGE
            jobInfo.add(cursor.getString(7)); //LUCK_RANGE
            jobInfo.add(cursor.getString(8)); //SKILL_GROUP_ID
            jobInfo.add(cursor.getString(9)); //JA
            jobInfo.add(cursor.getString(10)); //JOB_INFO

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return jobInfo;
    }

    //PAIDからPA情報を取得
    public ArrayList<String> GetPAInfo(int PAID){

        ArrayList<String> paInfo = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + PA_TABLE +
                            " WHERE " + COLUMN_PA_ID + " ='" + PAID + "';",
                    null
            );

            cursor.moveToFirst();

            paInfo.add(cursor.getString(1)); //PA_NAME
            paInfo.add(cursor.getString(2)); //PA_CON
            paInfo.add(cursor.getString(3)); //PA_EFF

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return paInfo;
    }

    //キャラクターデータをDBへ保存
    public void InsertCharacterData(String chara_name,
                                     int job_id, int hp, int str, int def, int agi, int dex, int luck,
                                     int pa_id){
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_CHARA_NAME, chara_name);
            values.put(COLUMN_JOB_ID, job_id);
            values.put(COLUMN_HP, hp);
            values.put(COLUMN_STR, str);
            values.put(COLUMN_DEF, def);
            values.put(COLUMN_AGI, agi);
            values.put(COLUMN_DEX, dex);
            values.put(COLUMN_LUCK, luck);
            values.put(COLUMN_PA_ID, pa_id);
            values.put(COLUMN_CREATE_AT, new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
            db.insert(CHARACTER_TABLE, null, values);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    //保存された全てのキャラのキャラクターIDを取得
    public ArrayList<Integer> GetAllCharacterID(){
        ArrayList<Integer> charaId = new ArrayList<Integer>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + _CHARA_ID + " FROM " + CHARACTER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                charaId.add(cursor.getInt(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return charaId;
    }

    //保存された全てのキャラのキャラクター名を取得
    public ArrayList<String> GetAllCharacterName(){
        ArrayList<String> charaId = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_CHARA_NAME + " FROM " + CHARACTER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                charaId.add(cursor.getString(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return charaId;
    }

    //保存された全てのキャラのジョブ名を取得
    public ArrayList<String> GetAllCharacterJob(){
        ArrayList<String> allJob = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_JOB_ID + " FROM " + CHARACTER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                ArrayList<String> jobInfo = GetJobInfo(Integer.valueOf(cursor.getString(0)));
                allJob.add(jobInfo.get(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return allJob;
    }

    //保存された全てのキャラのPAを取得
    public ArrayList<String> GetAllCharacterPA(){
        ArrayList<String> allPA = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_PA_ID + " FROM " + CHARACTER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                ArrayList<String> jobInfo = GetPAInfo(Integer.valueOf(cursor.getString(0)));
                allPA.add(jobInfo.get(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return allPA;
    }


    //指定されたキャラクター情報を取得
    public ArrayList<String> GetCharacterInfo(int charaID){
        ArrayList<String> charaInfo = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + CHARACTER_TABLE +
                            " WHERE " + _CHARA_ID + " ='" + charaID + "';",
                    null
            );

            cursor.moveToFirst();

            charaInfo.add(cursor.getString(0)); //CHARA_ID
            charaInfo.add(cursor.getString(1)); //CHARA_NAME
            charaInfo.add(cursor.getString(2)); //JOB_ID
            charaInfo.add(cursor.getString(3)); //HP
            charaInfo.add(cursor.getString(4)); //STR
            charaInfo.add(cursor.getString(5)); //DEF
            charaInfo.add(cursor.getString(6)); //AGI
            charaInfo.add(cursor.getString(7)); //DEX
            charaInfo.add(cursor.getString(8)); //LUCK
            charaInfo.add(cursor.getString(9)); //PA_ID
            charaInfo.add(cursor.getString(10)); //CREATE_AT

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return charaInfo;
    }

    //選択しているキャラクターデータを削除する
    public void DeleteCharacterData(int characterId){
        try {
            db.execSQL("DELETE FROM " + CHARACTER_TABLE + " WHERE " + _CHARA_ID + " = " + characterId + ";");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //指定されたエネミー情報を取得
    public ArrayList<String> GetEnemyInfo(int enemyID){

        ArrayList<String> enemyInfo = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + ENEMY_TABLE +
                            " WHERE " + COLUMN_ENEMY_ID + " ='" + enemyID + "';",
                    null
            );

            cursor.moveToFirst();

            enemyInfo.add(cursor.getString(0)); //ENEMY_ID
            enemyInfo.add(cursor.getString(1)); //ENEMY_NAME
            enemyInfo.add(cursor.getString(2)); //JOB_ID
            enemyInfo.add(cursor.getString(3)); //HP
            enemyInfo.add(cursor.getString(4)); //STR
            enemyInfo.add(cursor.getString(5)); //DEF
            enemyInfo.add(cursor.getString(6)); //AGI
            enemyInfo.add(cursor.getString(7)); //DEX
            enemyInfo.add(cursor.getString(8)); //LUCK
            enemyInfo.add(cursor.getString(9)); //PA_ID

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return enemyInfo;
    }

    //指定されたスキル情報を取得
    public ArrayList<String> GetSkillInfo(String skillId){

        ArrayList<String> skillInfo = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + SKILL_TABLE +
                            " WHERE " + COLUMN_SKILL_ID + " ='" + skillId + "';",
                    null
            );

            cursor.moveToFirst();

            skillInfo.add(cursor.getString(1)); //SKILL_NAME
            skillInfo.add(cursor.getString(2)); //SKILL_COST
            skillInfo.add(cursor.getString(3)); //SKILL_EFF

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return skillInfo;
    }

    //ジョブのスキルグループIDからそのジョブのスキル一覧を取得
    public ArrayList<Skill> GetAllSkill(String skillGroupId){
        ArrayList<Skill> allSkill = new ArrayList<Skill>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + SKILL_TABLE + " WHERE " + COLUMN_SKILL_ID + " LIKE '" + skillGroupId + "%';",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Skill skill = new Skill();
                skill.setSkillId(cursor.getString(0));
                skill.setSkillName(cursor.getString(1));
                skill.setSkillCost(cursor.getInt(2));
                skill.setSkillEff(cursor.getString(3));
                allSkill.add(skill);
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return allSkill;
    }

    //保存された全ての作戦IDを取得
    public ArrayList<String> GetAllOrderId(){
        ArrayList<String> allOrderId = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_ORDER_ID + " FROM " + ORDER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                allOrderId.add(cursor.getString(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return allOrderId;
    }

    //保存された全ての作戦名を取得
    public ArrayList<String> GetAllOrderName(){
        ArrayList<String> allOrderName = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT " + COLUMN_ORDER_NAME + " FROM " + ORDER_TABLE + ";",
                    null
            );

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                allOrderName.add(cursor.getString(0));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return allOrderName;
    }

    //OrderIDから作戦情報を取得
    public ArrayList<String> GetOrderInfo(int OrderID){

        ArrayList<String> orderInfo = new ArrayList<String>();

        try {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM " + ORDER_TABLE +
                            " WHERE " + COLUMN_ORDER_ID + " ='" + OrderID + "';",
                    null
            );

            cursor.moveToFirst();

            orderInfo.add(cursor.getString(1)); //ORDER_NAME
            orderInfo.add(cursor.getString(2)); //ATTACK_TARGET
            orderInfo.add(cursor.getString(3)); //ORDER_DESCRIPTION
            orderInfo.add(cursor.getString(4)); //ATTACK_PROBABILITY
            orderInfo.add(cursor.getString(5)); //GUARD_PROBABILITY
            orderInfo.add(cursor.getString(6)); //SKILL_USE_CHECK

            cursor.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return orderInfo;
    }
}
