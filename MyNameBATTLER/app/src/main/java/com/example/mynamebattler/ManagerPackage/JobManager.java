package com.example.mynamebattler.ManagerPackage;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.example.mynamebattler.DBPackage.DBManager;

import java.util.ArrayList;

/*
　ジョブ情報を管理するクラス
 */
public class JobManager {

    private DBManager dbManager;

    private static final JobManager INSTANCE = new JobManager();

    private JobManager(){
        dbManager = DBManager.getInstance();
    }

    //シングルトンパターン
    public static JobManager getInstance(){
        return INSTANCE;
    }

    //ジョブスピナーのアイテムを取得する
    public ArrayAdapter<String> GetJobSpinnerItem(Context context){
        return dbManager.GetJobSpinnerItem(context);
    }

    //ジョブ名からジョブIDを取得する
    public int GetJobID(String jobName){
        return dbManager.GetJobID(jobName);
    }

    //ジョブIDからジョブ情報を取得する
    public ArrayList<String> GetJobInfo(int jobId){
        return dbManager.GetJobInfo(jobId);
    }
}
