package com.example.mynamebattler.PartyPackage;

import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;
/*
　敵側のパーティークラス(Partyを継承)
 */
public class EnemyParty extends Party implements Serializable {

    public EnemyParty(LinkedList<PlayerData> party){
        super(party);
    }
    
}
