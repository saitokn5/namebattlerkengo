package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：ファイター(Playerクラスを継承)
 */
public class Fighter extends PlayerData implements Constant, Serializable {

    public Fighter(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_B) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_B;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_B;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_C;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_E;
    }

    //JA
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){

        //JA未発動且つHPが0以下になった時に発動
        if(!JACheck && battleHp <= 0){
            int JAHp = defaultHp / 10;
            battleHp = JAHp;
            actLog.append(getPlayerName() + "のJA発動！\n力を振り絞り立ち上がった！\n\n");
            setJACheck(true);
        }
    }

}
