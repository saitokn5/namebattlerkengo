package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.OrderManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;

import java.util.ArrayList;

/*
　バトルメニュー画面の表示及びUI処理を行うクラス
 */
public class BattleMenuActivity extends AppCompatActivity implements Constant {

    private SoundManager soundManager;
    private ImageManager imageManager;
    private OrderManager orderManager;
    private BattleManager battleManager;
    private TextView battleLog,orderTargetText,orderActionText;
    private Spinner orderSpinner;
    private ArrayList<String> allOrderId, allOrderName,orderInfo;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battle_menu_activity);

        //各種インスタンスを取得
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();
        battleManager = BattleManager.getInstance();
        orderManager = OrderManager.getInstance();

        soundManager.BGMPause();
        InitButtonUI();
        InitSpinner();
        InitBattleLog();

    }

    //バトルログの表示
    private void InitBattleLog(){
        battleLog = findViewById(R.id.battleLgoText);

        //バトルログへテキストを反映
        battleLog.setText(battleManager.getBattleLog().toString());
    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //バックボタン押下時の動作
        ImageButton backBtn = findViewById(R.id.BackButton);
        Bitmap btnImage1 = imageManager.GetImageBitmap(this,BACK_BUTTON_IMAGE);
        backBtn.setImageBitmap(btnImage1);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //バトル結果画面へ遷移(ギブアップ)
        ImageButton giveUpBtn = findViewById(R.id.SendBattleReadyActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.GIVE_UP_BUTTON_IMAGE);
        giveUpBtn.setImageBitmap(btnImage2);
        giveUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),BattleResultActivity.class);
                startActivity(intent);
            }
        });

    }

    //スピナーの設定
    private void InitSpinner(){

        //IDリストを取得
        allOrderId = orderManager.GetOrderIdList();

        //スピナー
        orderSpinner = findViewById(R.id.orderSpinner);

        //作戦名一覧を取得し、アダプターを作成
        allOrderName = new ArrayList<String>();
        allOrderName = orderManager.GetOrderNameList();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, allOrderName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //スピナーにアダプターをセット
        orderSpinner.setAdapter(adapter);

        //スピナーで選択した作戦を反映
        orderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectOrderId = Integer.valueOf(allOrderId.get(position));
                orderManager.setOrder(battleManager.getMyParty(),selectOrderId);
                orderInfo = orderManager.GetOrderInfo(selectOrderId);
                InitOrderText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //作戦テキストの表示
    private void InitOrderText(){
        orderTargetText = findViewById(R.id.OrderTargetText);
        orderActionText = findViewById(R.id.OrderActionText);

        orderTargetText.setText(orderInfo.get(1));
        orderActionText.setText(orderInfo.get(2));

    }
}
