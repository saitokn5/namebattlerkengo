package com.example.mynamebattler.ManagerPackage;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
/*
* パーティー編成の処理＆管理を行うクラス
 */
public class PartyManager implements Constant {

    private static final PartyManager INSTANCE = new PartyManager();
    private LinkedList<PlayerData> myParty;
    private LinkedList<PlayerData> enemyParty;

    private CharacterDataManager characterDataManager;

    //シングルトンパターン
    private PartyManager(){
        characterDataManager = CharacterDataManager.getInstance();
    }

    public static PartyManager getInstance(){
        return INSTANCE;
    }

    //getterここから
    public LinkedList<PlayerData> getMyParty() {
        return myParty;
    }

    public LinkedList<PlayerData> getEnemyParty() {
        return enemyParty;
    }

    //getterここまで

    //パーティー情報の初期化
    public void ResetParty(){
        myParty = new LinkedList<PlayerData>();
        enemyParty = new LinkedList<PlayerData>();
    }

    //選択したキャラをパーティーに加える
    public void InitMyParty(PlayerData playerData) {

        //パーティーの人数に応じて処理を分岐
        //パーティーが空なら追加のみして終了
        if (myParty == null) {
            myParty.add(playerData);
            return;
        } else {
            //選択したキャラが既に編成済みかどうかチェック
            for (int i = 0; i < myParty.size(); i++) {
                //既に編成済みの場合は編成から解除して終了
                if (playerData.getCharaId() == myParty.get(i).getCharaId()) {
                    myParty.remove(i);
                    return;
                }
            }

            //パーティーが1以上且つ重複出ない場合
            if (myParty.size() < PARTY_INIT_LIMIT) {
                //パーティーが1～上限未満の場合は追加
                myParty.add(playerData);
            } else {
                //パーティー上限の場合は最初に選択されたキャラを編成から解除してから追加
                myParty.removeFirst();
                myParty.add(playerData);
            }
        }
    }

    //エネミーのパーティーを生成する
    public void EnemyPartyCreate(){
        DBManager dbManager = DBManager.getInstance();

        //エネミー側のキャラクターIDをランダムに決定
        Random random = new Random();
        int enemy1Id = random.nextInt(80) + 1001;
        int enemy2Id = random.nextInt(80) + 1001;
        int enemy3Id = random.nextInt(80) + 1001;
        ArrayList<String> enemyinfo = new ArrayList<String>();

        //エネミー1を作成
        enemyinfo = dbManager.GetEnemyInfo(enemy1Id);
        PlayerData enemy1 = characterDataManager.GetCharacterData(enemy1Id,Integer.valueOf(enemyinfo.get(2)),false);
        enemyParty.add(enemy1);

        //エネミー2を作成
        enemyinfo = dbManager.GetEnemyInfo(enemy2Id);
        PlayerData enemy2 =characterDataManager.GetCharacterData(enemy2Id,Integer.valueOf(enemyinfo.get(2)),false);
        enemyParty.add(enemy2);

        //エネミー3を作成
        enemyinfo = dbManager.GetEnemyInfo(enemy3Id);
        PlayerData enemy3 = characterDataManager.GetCharacterData(enemy3Id,Integer.valueOf(enemyinfo.get(2)),false);
        enemyParty.add(enemy3);

    }

    //エネミーのパーティーを更新する
    public void EnemyPartyUpdate(){
        enemyParty.clear();
        EnemyPartyCreate();
    }

    //各パーティーのキャラクター状態を初期化する
    public void ResetAllPartyMember(){

        //味方側のキャラクター情報を初期化
        PlayerData myMember1 = characterDataManager.GetCharacterData(myParty.get(0).getCharaId(),myParty.get(0).getJobId(),true);
        PlayerData myMember2 =characterDataManager.GetCharacterData(myParty.get(1).getCharaId(),myParty.get(1).getJobId(),true);
        PlayerData myMember3 = characterDataManager.GetCharacterData(myParty.get(2).getCharaId(),myParty.get(2).getJobId(),true);

        //味方パーティーを初期化
        myParty.clear();
        myParty.add(myMember1);
        myParty.add(myMember2);
        myParty.add(myMember3);

        //エネミー側のキャラクター情報を初期化
        PlayerData enemy1 = characterDataManager.GetCharacterData(enemyParty.get(0).getCharaId(),enemyParty.get(0).getJobId(),false);
        PlayerData enemy2 =characterDataManager.GetCharacterData(enemyParty.get(1).getCharaId(),enemyParty.get(1).getJobId(),false);
        PlayerData enemy3 = characterDataManager.GetCharacterData(enemyParty.get(2).getCharaId(),enemyParty.get(2).getJobId(),false);

        //エネミーパーティーを初期化
        enemyParty.clear();
        enemyParty.add(enemy1);
        enemyParty.add(enemy2);
        enemyParty.add(enemy3);
    }
}
