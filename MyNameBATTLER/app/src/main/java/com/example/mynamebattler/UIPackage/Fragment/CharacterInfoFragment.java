package com.example.mynamebattler.UIPackage.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.R;
import com.example.mynamebattler.SkillPackage.Skill;

import java.util.ArrayList;

/*
　CharacterInfoFragmentを管理するクラス
 */
public class CharacterInfoFragment extends Fragment implements Constant {

    private Bundle bundle;
    private ArrayList<String> charaInfo,jobInfo,PAInfo;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;


    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.character_info_fragment,container,false);
        return rootView;

    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InitCharacterList(view);
        InitButtonUI(view);
    }

    //リストの表示
    private void InitCharacterList(View view){
        //バンドルから各種データを取得
        bundle = getArguments();
        charaInfo = bundle.getStringArrayList(CHARACTER_INFO_LIST_KEY);
        jobInfo = bundle.getStringArrayList(JOB_INFO_LIST_KEY);
        PAInfo = bundle.getStringArrayList(PA_INFO_LIST_KEY);
        allSkillId = bundle.getStringArrayList(SKILL_ID_LIST_KEY);
        allSkillName = bundle.getStringArrayList(SKILL_NAME_LIST_KEY);
        allSkillCost = bundle.getIntegerArrayList(SKILL_COST_LIST_KEY);
        allSkillEff = bundle.getStringArrayList(SKILL_EFF_LIST_KEY);

        //キャラクター名の表示
        TextView characterNameText = view.findViewById(R.id.CharacterNameText);
        characterNameText.setText("：" + charaInfo.get(1));

        //職業の表示
        TextView jobText = view.findViewById(R.id.JobText);
        jobText.setText("：" + jobInfo.get(0));

        //HPの表示
        TextView hpText = view.findViewById(R.id.HPText);
        hpText.setText("：" + charaInfo.get(3));

        //STRの表示
        TextView strText = view.findViewById(R.id.StrText);
        strText.setText("：" + charaInfo.get(4));

        //DEFの表示
        TextView defText = view.findViewById(R.id.DefText);
        defText.setText("：" + charaInfo.get(5));

        //AGIの表示
        TextView agiText = view.findViewById(R.id.AgiText);
        agiText.setText("：" + charaInfo.get(6));

        //DEXの表示
        TextView dexText = view.findViewById(R.id.DexText);
        dexText.setText("：" + charaInfo.get(7));

        //LUCKの表示
        TextView luckText = view.findViewById(R.id.LuckText);
        luckText.setText("：" + charaInfo.get(8));

        //PA情報の表示
        TextView PAText = view.findViewById(R.id.PAText);
        TextView PADexText = view.findViewById(R.id.PADesText);
        PAText.setText("：" + PAInfo.get(0));
        PADexText.setText(PAInfo.get(1) + "\n" + PAInfo.get(2));

        //キャラクター作成日の表示
        TextView createAtText = view.findViewById(R.id.createAtText);
        createAtText.setText("：" + charaInfo.get(10));
    }

    private void InitButtonUI(View view){

        //スキル一覧ボタンを押下時の動作
        Button skillListButton = view.findViewById(R.id.SkillListButton);
        skillListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Bundleに選択している職業のスキル一覧を格納
                bundle.putStringArrayList(SKILL_ID_LIST_KEY,allSkillId);
                bundle.putStringArrayList(SKILL_NAME_LIST_KEY,allSkillName);
                bundle.putIntegerArrayList(SKILL_COST_LIST_KEY,allSkillCost);
                bundle.putStringArrayList(SKILL_EFF_LIST_KEY,allSkillEff);

                //skillListDialogFragmentを表示
                DialogFragment skillListDialogFragment = new SkillListDialogFragment();
                skillListDialogFragment.setArguments(bundle);
                skillListDialogFragment.show(getFragmentManager(),"skill_list_dialog");
            }
        });
    }
}
