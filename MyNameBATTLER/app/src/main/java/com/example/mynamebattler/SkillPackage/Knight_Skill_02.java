package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ナイト - スキル2
　かばう
 */
public class Knight_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName+ "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);

            //パーティーのcoveredTargetに自身の情報をセット
            myParty.setCoveredTarget(actionMember);

            actionMember.setActLog("相手のターゲットを自身に集中させた！\n\n");
        }
    }
}
