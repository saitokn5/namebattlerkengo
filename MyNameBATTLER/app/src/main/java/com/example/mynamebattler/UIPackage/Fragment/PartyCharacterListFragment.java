package com.example.mynamebattler.UIPackage.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.AdapterPackage.CharacterListViewAdapter;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.ManagerPackage.JobManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.PartyPackage.MyParty;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;

import java.util.ArrayList;

/*
　パーティー編成画面でCharacterListFragmentを管理するクラス
 */
public class PartyCharacterListFragment extends Fragment implements Constant, AdapterView.OnItemClickListener {

    private Bundle bundle;
    private ArrayList<Integer> allCharaId;
    private ArrayList<String> allCharaName,allCharaJob,allCharaPA;

    private Party myParty;

    private CharacterDataManager characterDataManager;
    private JobManager jobManager;
    private PartyManager partyManager;
    private MyPartyFragment myPartyFragment;

    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.chararacter_list_fragment,container,false);

        //各マネージャーのインスタンスを取得
        characterDataManager = CharacterDataManager.getInstance();
        jobManager = JobManager.getInstance();
        partyManager = PartyManager.getInstance();
        return rootView;

    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InitCharacterList(view);
    }

    //リストの表示
    private void InitCharacterList(View view){
        //バンドルから各種データを取得
        bundle = getArguments();
        allCharaId = bundle.getIntegerArrayList(CHARACTER_ID_LIST_KEY);
        allCharaName = bundle.getStringArrayList(CHARACTER_NAME_LIST_KEY);
        allCharaJob = bundle.getStringArrayList(CHARACTER_JOB_LIST_KEY);
        allCharaPA = bundle.getStringArrayList(CHARACTER_PA_LIST_KEY);

        //ListViewの設定
        ListView characterList = view.findViewById(R.id.CharacterList);
        BaseAdapter adapter = new CharacterListViewAdapter(view.getContext(),R.layout.character_list_item,
                allCharaId,
                allCharaName,
                allCharaJob,
                allCharaPA);
        characterList.setAdapter(adapter);
        characterList.setOnItemClickListener(this);
    }

    //リストを押下時の動作
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id){

        int characterId = allCharaId.get(position);
        int jobId = jobManager.GetJobID(allCharaJob.get(position));

        //押下したリストのIDを基にキャラクター情報を取得
        PlayerData playerData = characterDataManager.GetCharacterData(characterId,jobId,true);

        //取得したキャラクターをパーティーに追加
        partyManager.InitMyParty(playerData);

        //現時点のパーティー情報を取得
        myParty = new MyParty(partyManager.getMyParty());

        //現在の編成人数に応じてBundleへパーティー情報を格納
        switch (myParty.getParty().size()){
            case 0:
                bundle.putSerializable(MYPARTY_PLAYER1_KEY,null);
                bundle.putSerializable(MYPARTY_PLAYER2_KEY,null);
                bundle.putSerializable(MYPARTY_PLAYER3_KEY,null);
                break;
            case 1:
                bundle.putSerializable(MYPARTY_PLAYER1_KEY,myParty.getParty().get(0));
                bundle.putSerializable(MYPARTY_PLAYER2_KEY,null);
                bundle.putSerializable(MYPARTY_PLAYER3_KEY,null);
                break;
            case 2:
                bundle.putSerializable(MYPARTY_PLAYER1_KEY,myParty.getParty().get(0));
                bundle.putSerializable(MYPARTY_PLAYER2_KEY,myParty.getParty().get(1));
                bundle.putSerializable(MYPARTY_PLAYER3_KEY,null);
                break;
            case 3:
                bundle.putSerializable(MYPARTY_PLAYER1_KEY,myParty.getParty().get(0));
                bundle.putSerializable(MYPARTY_PLAYER2_KEY,myParty.getParty().get(1));
                bundle.putSerializable(MYPARTY_PLAYER3_KEY,myParty.getParty().get(2));
                break;
        }

        //MyPartyFragmentにパーティーを送り、更新
        myPartyFragment = new MyPartyFragment();
        myPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mypartyFlame,myPartyFragment);
        transaction.commit();
    }
}
