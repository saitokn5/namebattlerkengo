package com.example.mynamebattler.UIPackage.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.JobManager;
import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;
import com.example.mynamebattler.SkillPackage.Skill;
import com.example.mynamebattler.UIPackage.Fragment.CharacterInfoFragment;
import com.example.mynamebattler.UIPackage.Fragment.HeaderFragment;

import java.util.ArrayList;
/*
　キャラクター情報画面の表示及びUI処理を行うクラス
 */
public class CharacterInfoActivity extends AppCompatActivity implements Constant {

    private ArrayList<String> charaInfo,jobInfo,PAInfo;
    private int charaId;

    private SoundManager soundManager;
    private ImageManager imageManager;
    private SkillManager skillManager;
    private CharacterDataManager characterDataManager;
    private PAManager paManager;
    private JobManager jobManager;

    private Bundle bundle;
    private CharacterInfoFragment characterInfoFragment;
    private HeaderFragment headerFragment;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_info_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスの取得
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();
        skillManager = SkillManager.getInstance();
        characterDataManager = CharacterDataManager.getInstance();
        paManager = PAManager.getInstance();
        jobManager = JobManager.getInstance();

        GetCharacterData();
        InitHeaderFragment();
        InitCharacterInfoFragment();
        InitButtonUI();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //インテントで送られたキャラIDを基にDBからキャラ情報を取得し、バンドルへ格納
    private void GetCharacterData(){
        Intent intent = getIntent();
        charaId = intent.getIntExtra(_CHARA_ID,0);

        //キャラクター情報の取得
        charaInfo = characterDataManager.GetCharacterInfo(charaId);
        bundle.putStringArrayList(CHARACTER_INFO_LIST_KEY,charaInfo);

        //職業情報の取得
        jobInfo = jobManager.GetJobInfo(Integer.valueOf(charaInfo.get(2)));
        bundle.putStringArrayList(JOB_INFO_LIST_KEY,jobInfo);

        //PA情報の取得
        PAInfo = paManager.GetPAInfo(Integer.valueOf(charaInfo.get(9)));
        bundle.putStringArrayList(PA_INFO_LIST_KEY,PAInfo);

        //スキル情報の取得
        ArrayList<Skill> allSkill = new ArrayList<Skill>();
        allSkill = skillManager.GetSkillListItem(Integer.valueOf(charaInfo.get(2)));

        allSkillId = new ArrayList<String>();
        allSkillName = new ArrayList<String>();
        allSkillCost = new ArrayList<Integer>();
        allSkillEff = new ArrayList<String>();

        for(Skill skill:allSkill){
            allSkillId.add(skill.getSkillId());
            allSkillName.add(skill.getSkillName());
            allSkillCost.add(skill.getSkillCost());
            allSkillEff.add(skill.getSkillEff());
        }

        //Bundleに選択している職業のスキル一覧を格納
        bundle.putStringArrayList(SKILL_ID_LIST_KEY,allSkillId);
        bundle.putStringArrayList(SKILL_NAME_LIST_KEY,allSkillName);
        bundle.putIntegerArrayList(SKILL_COST_LIST_KEY,allSkillCost);
        bundle.putStringArrayList(SKILL_EFF_LIST_KEY,allSkillEff);
    }

    //characterInfoFragmentの設定＆表示
    private void InitCharacterInfoFragment(){

        //characterInfoFragmentを作成して表示
        characterInfoFragment = new CharacterInfoFragment();
        characterInfoFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.characterInfoFlame,characterInfoFragment);
        transaction.commit();

    }

    //HeaderFragmentの表示
    private void InitHeaderFragment(){
        bundle.putString(BACK_ACT_CHECK_KEY,BACK_CHARACTER_LIST_ACT_KEY);
        headerFragment = new HeaderFragment();
        headerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.headerFragmentFrame,headerFragment);
        transaction.commit();
    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //削除ボタン押下時の動作
        ImageButton deleteCharacterBtn = findViewById(R.id.DeleteCharacterButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.DELETE_CHARACTER_BUTTON_IMAGE);
        deleteCharacterBtn.setImageBitmap(btnImage2);
        deleteCharacterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(CharacterInfoActivity.this)
                        .setTitle(R.string.ConfirmationDialogTitle)
                        .setMessage(R.string.DeleteDialogText)
                        .setPositiveButton(R.string.DialogOptionYes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //ダイアログではいを押下時にキャラクターを削除
                                characterDataManager.DeleteCharacterData(charaId);
                                Intent intent = new Intent(getApplication(),CharacterListActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(R.string.DialogOptionNo,null)
                        .show();
            }
        });
    }
}
