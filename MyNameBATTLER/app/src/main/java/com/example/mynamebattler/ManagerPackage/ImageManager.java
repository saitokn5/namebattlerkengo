package com.example.mynamebattler.ManagerPackage;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.mynamebattler.Constant;

import java.io.InputStream;
/*
　画像の管理を行うクラス
 */
public class ImageManager implements Constant {

    private static final ImageManager INSTANCE = new ImageManager();

    private ImageManager(){

    }

    //シングルトンパターン
    public static ImageManager getInstance(){
        return INSTANCE;
    }

    //Assetsから画像データを取得
    public Bitmap GetImageBitmap(Context context, String fileName){

        AssetManager assetManager = context.getResources().getAssets();
        try(InputStream inputStream = assetManager.open(fileName)){
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    //バトルリザルト画像の取得(勝敗により画像を変更)
    public Bitmap GetBattleResultImage(Context context, boolean you_win){

        String fileName;
        if(you_win){
            fileName = YOU_WIN_IMAGE_FILENAME;
        } else {
            fileName = YOU_LOSE_IMAGE_FILENAME;
        }

        AssetManager assetManager = context.getResources().getAssets();
        try(InputStream inputStream = assetManager.open(fileName)){
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
