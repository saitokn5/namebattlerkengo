package com.example.mynamebattler.UIPackage.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Activity.CharacterInitActivity;
import com.example.mynamebattler.UIPackage.Activity.CharacterListActivity;
import com.example.mynamebattler.UIPackage.Activity.PartyFormationActivity;
import com.example.mynamebattler.UIPackage.Activity.TitleActivity;

public class HeaderFragment extends Fragment implements Constant {

    private ImageButton backButton;
    private Bundle bundle;
    private String backActKey;

    //Viewの生成(Fragment更新時に必ず更新される様に既存のViewをremove後、設定する)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        super.onCreateView(inflater,container,savedInstanceState);
        container.removeAllViews();
        View rootView = inflater.inflate(R.layout.header_fragment,container,false);
        return rootView;
    }

    //View生成後の処理
    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

        //Bundleから値を取得
        bundle = getArguments();
        backActKey = bundle.getString(BACK_ACT_CHECK_KEY);

        InitBuckButton();
    }

    //backButtonの設定＆処理
    private void InitBuckButton(){
        backButton = getActivity().findViewById(R.id.backButton);

        //押下した時の動作(画面に応じて戻る画面を判定し、画面を戻る)
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (backActKey){
                    case BACK_TITLE_ACT_KEY:
                        intent = new Intent(getActivity(), TitleActivity.class);
                        startActivity(intent);
                        break;
                    case BACK_CHARACTER_LIST_ACT_KEY:
                        intent = new Intent(getActivity(), CharacterListActivity.class);
                        startActivity(intent);
                        break;
                    case BACK_PARTY_FORMATION_ACT_KEY:
                        intent = new Intent(getActivity(), PartyFormationActivity.class);
                        startActivity(intent);
                        break;


                }
            }
        });
    }

}
