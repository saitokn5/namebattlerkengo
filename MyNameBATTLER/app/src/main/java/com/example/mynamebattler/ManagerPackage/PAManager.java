package com.example.mynamebattler.ManagerPackage;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.PAPackage.BasePA;
import com.example.mynamebattler.PAPackage.PA201_REGENERATE;
import com.example.mynamebattler.PAPackage.PA202_POISON_ATTACK;
import com.example.mynamebattler.PAPackage.PA203_TAC_COM;
import com.example.mynamebattler.PAPackage.PA204_CRISIS_POWER;
import com.example.mynamebattler.PAPackage.PA205_SLOW_STARTER;
import com.example.mynamebattler.PAPackage.PA206_PRESSURE;
import com.example.mynamebattler.PAPackage.PA207_FORERUNNER;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.ArrayList;

/*
　PAの発動を管理するクラス
 */
public class PAManager implements Serializable {

    private DBManager dbManager;

    private static final PAManager INSTANCE = new PAManager();

    private PAManager(){
        dbManager = DBManager.getInstance();
    }

    //シングルトンパターン
    public static PAManager getInstance(){
        return INSTANCE;
    }

    private BasePA pa;

    //受け取ったPAIDに応じたPAを発動
    public void InvokePA(int PAID, PlayerData actionMember, Party myParty, Party enemyParty){
        switch (PAID){
            case 201:
                pa = new PA201_REGENERATE();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 202:
                pa = new PA202_POISON_ATTACK();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 203:
                pa = new PA203_TAC_COM();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 204:
                pa = new PA204_CRISIS_POWER();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 205:
                pa = new PA205_SLOW_STARTER();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 206:
                pa = new PA206_PRESSURE();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            case 207:
                pa = new PA207_FORERUNNER();
                pa.Invoke(actionMember,myParty,enemyParty);
                break;
            default:
                return;
        }
    }

    //指定したPAIDからPA情報を取得する
    public ArrayList<String> GetPAInfo(int PAId){
        return dbManager.GetPAInfo(PAId);
    }

    //PAスピナーのアイテムを取得
    public ArrayAdapter<String> GetPASpinnerItem(Context context){
        return dbManager.GetPASpinnerItem(context);
    }

    //PA名からPAIDを取得
    public int GetPAID(String paName){
        return dbManager.GetPAID(paName);
    }

}
