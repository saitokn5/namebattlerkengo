package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　PA：207　一番槍
【発動条件】戦闘開始時
　戦闘開始時の初期SP+20＆1ターン目のみ自身のAGL50％アップ
 */
public class PA207_FORERUNNER implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {

        //発動条件判定
        if(actionMember.getTurnCount() == 1){
            //1ターン目
            actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n先駆けて行動を開始した！\n\n");
            actionMember.setBattleAgi(actionMember.getBattleAgi() + (int)(actionMember.getDefaultAgi() * 0.5));
            myParty.SPCountUpdate(20);
            actionMember.setPaCheck(true);
        } else if(actionMember.getTurnCount() == 2){
            //2ターン目
            actionMember.setBattleAgi(actionMember.getBattleAgi() - (int)(actionMember.getDefaultAgi() * 0.5));
            actionMember.setPaCheck(false);
        }
    }
}
