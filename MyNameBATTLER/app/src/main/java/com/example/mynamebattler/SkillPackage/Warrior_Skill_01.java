package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ウォーリアー - スキル1
　捨て身の特攻
 */
public class Warrior_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(target.getPlayerName() + "へ超強力な一撃！\n");

            //スキル使用済みチェック
            if (actionMember.isSkillCheck()) {
                //前のターンで使用済みの場合(DEF・AGIは0のまま)
                //現在のSTRを2.5倍
                double str = actionMember.getBattleStr() * 2.5;
                actionMember.setBattleStr((int)str);

                //攻撃
                actionMember.Attack(actionMember, target);

                //STRを元に戻す
                actionMember.setBattleStr(actionMember.getDefaultStr());
            } else {

                //現在のSTRを2.5倍
                double str = actionMember.getBattleStr() * 2.5;
                actionMember.setBattleStr((int)str);

                //攻撃
                actionMember.Attack(actionMember, target);

                //STRを元に戻す
                actionMember.setBattleStr(actionMember.getDefaultStr());

                //DEF・AGIを0にする
                actionMember.setBattleDef(0);
                actionMember.setBattleAgi(0);
            }
            actionMember.setActLog("反動で" + actionMember.getPlayerName() + "は体勢を崩してしまった…\n\n");
        }
    }
}
