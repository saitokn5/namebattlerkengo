package com.example.mynamebattler.ManagerPackage;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.OrderPackage.BaseOrder;
import com.example.mynamebattler.OrderPackage.Order301;
import com.example.mynamebattler.OrderPackage.Order302;
import com.example.mynamebattler.OrderPackage.Order303;
import com.example.mynamebattler.OrderPackage.Order304;
import com.example.mynamebattler.OrderPackage.Order305;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;


import java.util.ArrayList;

/*
　作戦の処理を管理するクラス
 */
public class OrderManager implements Constant {

    private static final OrderManager INSTANCE = new OrderManager();

    private OrderManager(){
        //DBマネージャーのインスタンスを取得
        dbManager = DBManager.getInstance();
    }

    //シングルトンパターン
    public static OrderManager getInstance(){
        return INSTANCE;
    }

    private DBManager dbManager;

    //パーティーに作戦をセットする
    public void setOrder(Party setParty,int orderID){

        BaseOrder order;

        switch (orderID){
            case 301:
                order = new Order301();
                setOrderState(order,orderID); //作戦時の各種値をセット
                setParty.setOrder(order); //パーティーに作戦を設定
                break;
            case 302:
                order = new Order302();
                setOrderState(order,orderID); //作戦時の各種値をセット
                setParty.setOrder(order); //パーティーに作戦を設定
                break;
            case 303:
                order = new Order303();
                setOrderState(order,orderID); //作戦時の各種値をセット
                setParty.setOrder(order); //パーティーに作戦を設定
                break;
            case 304:
                order = new Order304();
                setOrderState(order,orderID); //作戦時の各種値をセット
                setParty.setOrder(order); //パーティーに作戦を設定
                break;
            case 305:
                order = new Order305();
                setOrderState(order,orderID); //作戦時の各種値をセット
                setParty.setOrder(order); //パーティーに作戦を設定
                break;
        }
    }

    //パーティーにセットされた作戦から行動内容を決定
    public int getAction(Party myParty){
        int actionId;
        int targetPoint = (int)(Math.random()*100); //0～99の目標値を決定
        int attackProbability = myParty.getOrder().getAttackProbability();

        if(myParty.getOrder().getSkillUseCheck(myParty)){
            actionId = SKILL_ID;
        } else {
            if(targetPoint < attackProbability){
                actionId = ATTACK_ID;
            } else {
                actionId = GUARD_ID;
            }
        }
        return actionId;
    }

    //作戦に応じたターゲットを取得
    public PlayerData getTarget(Party actParty,Party notActParty){
        return actParty.getOrder().getTarget(notActParty);
    }

    //DBから作戦ID一覧を取得
    public ArrayList<String> GetOrderIdList(){
        return dbManager.GetAllOrderId();
    }

    //DBから作戦名一覧を取得
    public ArrayList<String> GetOrderNameList(){
        return dbManager.GetAllOrderName();
    }

    //作戦IDを基にDBから作戦情報を取得
    public ArrayList<String> GetOrderInfo(int orderId){
        return dbManager.GetOrderInfo(orderId);
    }

    //
    private void setOrderState(BaseOrder order,int orderId){
        order.setOrderName(GetOrderInfo(orderId).get(0));
        order.setAttackProbability(Integer.valueOf(GetOrderInfo(orderId).get(3)));
        order.setGuardProbability(Integer.valueOf(GetOrderInfo(orderId).get(4)));
        order.setSkillUseCheck(Integer.valueOf(GetOrderInfo(orderId).get(5)));
    }

}
