package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.ArrayList;

/*
　PA：203　戦術指揮
 【発動条件】自パーティーのSP：50以上
  味方全体のAGI＆DEXを15％アップ
 */
public class PA203_TAC_COM implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {
        //発動条件判定
        if(myParty.getSp() >= 50) {
            //PA未発動の場合
            if(!actionMember.isPaCheck()){
                actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n" + actionMember.getPlayerName() + "の正確な指揮が冴えわたる！\n\n");
                //各キャラのステータスを加算値を計算し、加算
                for(int i = 0;i < myParty.getParty().size();i++){
                    PlayerData playerData = myParty.getParty().get(i);
                    playerData.setBattleAgi((int)(playerData.getBattleAgi() + playerData.getDefaultAgi() * 0.15));
                    playerData.setBattleDex((int)(playerData.getBattleDex() + playerData.getDefaultDex() * 0.15));
                }
                actionMember.setPaCheck(true);
            }
        } else {
            //SP50未満の場合
            //PA発動中の場合
            //ステータスの加算値を取る
            if(actionMember.isPaCheck()){
                for(int i = 0;i < myParty.getParty().size();i++){
                    PlayerData playerData = myParty.getParty().get(i);
                    playerData.setBattleAgi((int)(playerData.getBattleAgi() - playerData.getDefaultAgi() * 0.15));
                    playerData.setBattleDex((int)(playerData.getBattleDex() - playerData.getDefaultDex() * 0.15));
                }
                actionMember.setPaCheck(false);
            }
        }
    }
}
