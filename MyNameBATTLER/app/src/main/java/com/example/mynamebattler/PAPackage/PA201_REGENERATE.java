package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
/*
　PA：201　リジェネレート
　【発動条件】自パーティーのSP：40以上
　ターン開始時、自身の最大HPの3％分を回復
 */
public class PA201_REGENERATE implements BasePA,Serializable {

    @Override
    public void Invoke(PlayerData actionMember, Party myParty, Party enemyParty) {

        double recoveryHP = actionMember.getDefaultHp() * 0.03; //回復値

        //発動条件判定
        if(myParty.getSp() >= 40){
            actionMember.setActLog(actionMember.getPlayerName() + "のPA発動！\n体力が徐々に回復する！\n\n");
            //自身の最大HP3％分を回復
            double recoveredHP = actionMember.getBattleHp() + recoveryHP; //回復後のHP
            //回復後のHPがHP最大値を超える場合はHP最大値に丸め込み
            if (recoveredHP > actionMember.getDefaultHp()) {
                recoveryHP = actionMember.getDefaultHp() - actionMember.getBattleHp();
                recoveredHP = actionMember.getDefaultHp();
            }
            actionMember.setActLog(actionMember.getPlayerName() + "のHPが" + (int)recoveryHP +"回復した！\n\n");
            actionMember.setBattleHp((int) recoveredHP);
            actionMember.setPaCheck(true);
        } else {
            actionMember.setPaCheck(false);
        }
    }
}
