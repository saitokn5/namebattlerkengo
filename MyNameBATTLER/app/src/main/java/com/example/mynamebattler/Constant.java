package com.example.mynamebattler;
/*
　定数を管理
 */
public interface Constant {
    //インテント、バンドルなどで使用するキー
    String CHARACTER_NAME_KEY = "PlayerName"; //プレイヤーネームを他Activityに渡す際のキー
    String JOB_ID_KEY = "JOB_ID"; //ジョブIDを他Activityに渡す際のキー
    String JOB_NAME_KEY = "JOB_NAME"; //ジョブネームを他Activityに渡す際のキー
    String PA_ID_KEY = "PA_ID"; //PAIDを他Activityに渡す際のキー
    String MYPARTY_PLAYER1_KEY = "myParty_player1"; //MyPartyのプレイヤー1を他Activityに渡す際のキー
    String MYPARTY_PLAYER2_KEY = "myParty_player2"; //MyPartyのプレイヤー2を他Activityに渡す際のキー
    String MYPARTY_PLAYER3_KEY = "myParty_player3"; //MyPartyのプレイヤー3を他Activityに渡す際のキー
    String ENEMYPARTY_PLAYER1_KEY = "enemyParty_player1"; //EnemyPartyのプレイヤー1を他Activityに渡す際のキー
    String ENEMYPARTY_PLAYER2_KEY = "enemyParty_player2"; //EnemyPartyのプレイヤー2を他Activityに渡す際のキー
    String ENEMYPARTY_PLAYER3_KEY = "enemyParty_player3"; //EnemyPartyのプレイヤー3を他Activityに渡す際のキー
    String CHARACTER_ID_LIST_KEY = "characterIdListKey"; //キャラクターID一覧を他Activityに渡す際のキー
    String CHARACTER_NAME_LIST_KEY = "characterNameListKey"; //キャラクター名一覧を他Activityに渡す際のキー
    String CHARACTER_JOB_LIST_KEY = "characterJobListKey"; //キャラクタージョブ一覧を他Activityに渡す際のキー
    String CHARACTER_PA_LIST_KEY = "characterPAListKey"; //キャラクターPA一覧を他Activityに渡す際のキー
    String CHARACTER_INFO_LIST_KEY = "characterInfoListKey"; //キャラクター情報を他Activityに渡す際のキー
    String JOB_INFO_LIST_KEY = "jobInfoListKey"; //ジョブ情報を他Activityに渡す際のキー
    String PA_INFO_LIST_KEY = "PAInfoListKey"; //PA情報を他Activityに渡す際のキー
    String SKILL_ID_LIST_KEY = "skillIdListKey"; //スキルID一覧を他Activityに渡す際のキー
    String SKILL_NAME_LIST_KEY = "skillNameListKey"; //スキル名一覧を他Activityに渡す際のキー
    String SKILL_COST_LIST_KEY = "skillCostListKey"; //スキルコスト一覧を他Activityに渡す際のキー
    String SKILL_EFF_LIST_KEY = "skillEffListKey"; //スキル効果一覧を他Activityに渡す際のキー
    String ENEMY_LIST_KEY = "enemyListKey"; //スキル効果一覧を他Activityに渡す際のキー
    String COMMAND_KEY = "commandKey"; //選択したコマンドを他Activityに渡す際のキー
    String BACK_ACT_CHECK_KEY = "returnActivity"; //HeaderFragmentへ戻る画面情報を渡す為のキー
    String BACK_TITLE_ACT_KEY = "returnTitleActivity"; //HeaderFragmentでBackボタンを押下した時に戻る画面を判定するキー
    String BACK_CHARACTER_LIST_ACT_KEY = "returnCharacterListActivity"; //HeaderFragmentでBackボタンを押下した時に戻る画面を判定するキー
    String BACK_PARTY_FORMATION_ACT_KEY = "returnPartyFormationActivity"; //HeaderFragmentでBackボタンを押下した時に戻る画面を判定するキー

    //assets内のファイル名
    String TITLE_BGM_FILE = "sound/Title.mp3"; //タイトル画面BGMのファイル名
    String BATTLE_BGM_FILE = "sound/ShootingSky.mp3"; //バトル画面BGMのファイル名
    String CHARACTER_CREATE_BGM_FILE = "sound/RoutineWork.mp3"; //キャラクター作成画面BGMのファイル名
    String RESULT_BGM_FILE = "sound/Result.mp3"; //リザルト画面BGMのファイル名
    String TITLE_IMAGE_FILENAME = "image/title_image.png"; //タイトルロゴのファイル名
    String VS_IMAGE_FILENAME = "image/VS_image.png"; //VS画像のファイル名
    String YOU_WIN_IMAGE_FILENAME = "image/you_win_image.png"; //YouWin画像のファイル名
    String YOU_LOSE_IMAGE_FILENAME = "image/you_lose_image.png"; //YouLose画像のファイル名
    String BACK_BUTTON_IMAGE = "image/Back_button.png"; //ボタン画像のファイル名
    String BACK_TITLE_BUTTON_IMAGE = "image/BackTitle_button.png"; //ボタン画像のファイル名
    String BATTLE_BUTTON_IMAGE = "image/Battle_button.png"; //ボタン画像のファイル名
    String BATTLE_START_BUTTON_IMAGE = "image/BattleStart_button.png"; //ボタン画像のファイル名
    String CHARACTER_CREATE_BUTTON_IMAGE = "image/CharacterCreate_button_image.png"; //ボタン画像のファイル名
    String CHARACTER_LIST_BUTTON_IMAGE = "image/CharacterList_button.png"; //ボタン画像のファイル名
    String CREATE_BUTTON_IMAGE = "image/Create_button.png"; //ボタン画像のファイル名
    String CREATE_AGAIN_BUTTON_IMAGE = "image/CreateAgain_button.png"; //ボタン画像のファイル名
    String DELETE_CHARACTER_BUTTON_IMAGE = "image/DeleteCharacter_button.png"; //ボタン画像のファイル名
    String ENEMY_UPDATE_BUTTON_IMAGE = "image/EnemyUpdate_button.png"; //ボタン画像のファイル名
    String GIVE_UP_BUTTON_IMAGE = "image/GiveUp_button.png"; //ボタン画像のファイル名
    String NEW_CHARACTER_BUTTON_IMAGE = "image/NewCharacter_button.png"; //ボタン画像のファイル名
    String NEXT_BATTLE_BUTTON_IMAGE = "image/NextBattle_button.png"; //ボタン画像のファイル名
    String READY_BUTTON_IMAGE = "image/Ready_button.png"; //ボタン画像のファイル名
    String RETRY_BATTLE_BUTTON_IMAGE = "image/RetryBattle_button.png"; //ボタン画像のファイル名

    //レイアウト用の色コードなど
    String MY_PARTY_BACK_COLOR = "#add8e6"; //味方パーティーの背景色
    String ENEMY_PARTY_BACK_COLOR = "#ffb6c1"; //味方パーティーの背景色

    //ダメージ計算用の定数
    int STR_CONS = 3; //攻撃力の定数
    double DEF_CONS = 1.5; //防御力の定数
    double GUARD_CONS = 0.7; //防御時の軽減率
    double CRITICAL_CONS = 1.5; //クリティカル時の補正値

    //バトル時に使用する定数
    int SP_DEFAULT = 10;
    int ATTACK_SP_INCREASE = 5;
    int GUARD_SP_INCREASE = 15;
    int SP_LIMIT = 150; //SP最大値
    int PARTY_INIT_LIMIT = 3; //パーティーの編成人数
    int BAD_STATE_TURN = 3; //状態異常の継続ターン数

    //行動ダイアログの表y時
    String COMMAND_ATTACK = "攻撃";
    String COMMAND_GUARD = "防御";
    String COMMAND_SKILL = "スキル";

    //行動を決める際のID
    int ATTACK_ID = 0; //攻撃
    int GUARD_ID = 1; //防御
    int SKILL_ID = 2; //スキル

    //ステータス決定時の定数
    int STATUS_RANGE_BASE = 49; //ステータスレンジの基準値
    int STATUS_RANGE_A = 250; //ステータスAの加算値
    int STATUS_RANGE_B = 200; //ステータスBの加算値
    int STATUS_RANGE_C = 150; //ステータスCの加算値
    int STATUS_RANGE_D = 100; //ステータスDの加算値
    int STATUS_RANGE_E = 50; //ステータスEの加算値
    int STATUS_HP_FACTOR = 10; //HP算出時の係数

    //キャラクターテーブル情報
    String CHARACTER_TABLE = "CHARACTERS_TBL";
    String _CHARA_ID = "_chara_id";
    String COLUMN_CHARA_NAME = "chara_name";
    String COLUMN_JOB_ID = "_job_id";
    String COLUMN_HP = "hp";
    String COLUMN_STR = "str";
    String COLUMN_DEF = "def";
    String COLUMN_AGI = "agi";
    String COLUMN_DEX = "dex";
    String COLUMN_LUCK = "luck";
    String COLUMN_PA_ID = "_pa_id";
    String COLUMN_CREATE_AT = "create_at";

    //ジョブテーブル情報
    String JOB_TABLE = "JOB_TBL";
    String COLUMN_JOB_NAME = "job_name";
    String COLUMN_HP_RANGE = "hp_range";
    String COLUMN_STR_RANGE = "str_range";
    String COLUMN_DEF_RANGE = "def_range";
    String COLUMN_AGI_RANGE = "agi_range";
    String COLUMN_DEX_RANGE = "dex_range";
    String COLUMN_LUCK_RANGE = "luck_range";
    String COLUMN_SKILL_GROUP_ID = "skill_group_id";
    String COLUMN_JA = "ja";
    String COLUMN_JOB_INFO = "job_info";

    //PAテーブル情報
    String PA_TABLE = "PA_TBL";
    String COLUMN_PA_NAME = "pa_name";
    String COLUMN_PA_CON = "pa_con";
    String COLUMN_PA_EFF = "pa_eff";

    //エネミーテーブル情報
    String ENEMY_TABLE = "ENEMY_TBL";
    String COLUMN_ENEMY_ID = "_enemy_id";
    String COLUMN_ENEMY_NAME = "enemy_name";

    //オーダーテーブル情報
    String ORDER_TABLE = "ORDER_TBL";
    String COLUMN_ORDER_ID = "_ORDER_ID";
    String COLUMN_ORDER_NAME = "ORDER_NAME";
    String COLUMN_ATTACK_TARGET = "ATTACK_TARGET";
    String COLUMN_AUTO_ACTION = "ORDER_DESCRIPTION";

    //スキルテーブル情報
    String SKILL_TABLE = "SKILL_TBL";
    String COLUMN_SKILL_ID = "_skill_id";
    String COLUMN_SKILL_NAME = "skill_name";
    String COLUMN_SKILL_COST = "skill_cost";
    String COLUMN_SKILL_EFF = "skill_eff";

    //ジョブID
    int FIGHTER_ID = 101;
    int WIZARD_ID = 102;
    int CLERIC_ID = 103;
    int KNIGHT_ID = 104;
    int HUNTER_ID = 105;
    int WARRIOR_ID = 106;
    int CHEATER_ID = 107;

}
