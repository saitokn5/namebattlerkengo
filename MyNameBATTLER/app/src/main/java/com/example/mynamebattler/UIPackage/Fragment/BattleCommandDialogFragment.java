package com.example.mynamebattler.UIPackage.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.example.mynamebattler.AdapterPackage.SkillListViewAdapter;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;

import java.util.ArrayList;
/*
　バトルコマンドダイアログを管理するクラス
 */
public class BattleCommandDialogFragment extends DialogFragment implements Constant {

    private View view;

    private Bundle bundle;
    private String[] commandItems;
    private BattleManager battleManager;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        //各種インスタンスの取得
        battleManager = BattleManager.getInstance();

        //ダイアログビルダーを作成
        AlertDialog.Builder battleCommandBuilder = new AlertDialog.Builder(getActivity());

        //viewの取得
        view = LayoutInflater.from(getActivity()).inflate(R.layout.battle_command_fragment,null);

        GetListItems();

        battleCommandBuilder.setView(view)
                .setCustomTitle(GetCustomTitle(view))
                .setItems(commandItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //選択したコマンドにより各処理を実行
                        switch (commandItems[which]){
                            //攻撃
                            case COMMAND_ATTACK:
                                //攻撃の処理を書く
                                //ターゲット選択ダイアログに移行
                                bundle.putString(COMMAND_KEY,COMMAND_ATTACK);
                                DialogFragment targetCommandDialogFragment = new TargetCommandDialogFragment();
                                targetCommandDialogFragment.setArguments(bundle);
                                targetCommandDialogFragment.show(getFragmentManager(),"target_command_dialog");
                                break;
                            case COMMAND_GUARD:
                                Log.i("コマンド","防御");
                                battleManager.ActionFixed(Constant.GUARD_ID);
                                break;
                            case COMMAND_SKILL:
                                //スキルの処理
                                DialogFragment skillCommandListFragment = new SkillCommandListFragment();
                                skillCommandListFragment.setArguments(bundle);
                                skillCommandListFragment.show(getFragmentManager(),"skill_command_dialog");
                                break;
                        }
                    }
                })
                .setNegativeButton("閉じる",null);

        return battleCommandBuilder.create();
    }

    //ダイアログのタイトルを作成
    private TextView GetCustomTitle(View view){
        TextView customTitle = new TextView(view.getContext());
        customTitle.setText(R.string.BattleCommandTile); //表示テキスト
        customTitle.setGravity(Gravity.CENTER_HORIZONTAL); //中央揃え
        customTitle.setTextSize(24);
        customTitle.setTypeface(Typeface.DEFAULT_BOLD); //太字

        return customTitle;
    }

    private void GetListItems(){

        //コマンドの選択肢を設定
        commandItems = new String[]{COMMAND_ATTACK, COMMAND_GUARD, COMMAND_SKILL};

        //ターゲット表示用のデータを保持
        bundle = getArguments();
        ArrayList<String> targets = bundle.getStringArrayList(ENEMY_LIST_KEY);
        bundle.putStringArrayList(ENEMY_LIST_KEY,targets);

    }
}
