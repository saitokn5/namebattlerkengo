package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/*
　クレリック - スキル1
　ヒーリング
 */
public class Cleric_Skill_01 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);

            //JA発動中なら味方全体、未発動なら残り体力が一番少ない味方を回復
            if (actionMember.isJACheck()) {
                for (PlayerData playerData : myParty.getParty()) {
                    double recoveryHP = playerData.getDefaultHp() * 0.2; //回復値
                    double recoveredHP = playerData.getBattleHp() + recoveryHP; //回復後のHP

                    //回復後のHPがHP最大値を超える場合はHP最大値に丸め込み
                    if (recoveredHP > playerData.getDefaultHp()) {
                        recoveryHP = playerData.getDefaultHp() - playerData.getBattleHp();
                        recoveredHP = playerData.getDefaultHp();
                    }

                    //回復
                    actionMember.setActLog(playerData.getPlayerName() + "のHPを" + (int)recoveryHP +"回復した！\n\n");
                    playerData.setBattleHp((int) recoveredHP);
                }
            } else {
                //残り体力が少ない順に並び替え
                LinkedList<PlayerData> list = myParty.getParty();
                Collections.sort(list, new Comparator<PlayerData>() {
                    @Override
                    public int compare(PlayerData o1, PlayerData o2) {
                        return o1.getBattleHp() - o2.getBattleHp();
                    }
                });

                //一番残り体力が少ないキャラのHPを回復
                double recoveryHP = list.get(0).getDefaultHp() * 0.2; //回復値
                double recoveredHP = list.get(0).getBattleHp() + recoveryHP; //回復後のHP

                //回復後のHPがHP最大値を超える場合はHP最大値に丸め込み
                if (recoveredHP > list.get(0).getDefaultHp()) {
                    recoveryHP = list.get(0).getDefaultHp() - list.get(0).getBattleHp();
                    recoveredHP = list.get(0).getDefaultHp();
                }

                //回復
                actionMember.setActLog(list.get(0).getPlayerName() + "のHPを" + (int)recoveryHP + "回復した！\n\n");
                list.get(0).setBattleHp((int) recoveredHP);
            }
        }
    }
}
