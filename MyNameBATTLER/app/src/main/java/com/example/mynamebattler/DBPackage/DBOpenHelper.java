package com.example.mynamebattler.DBPackage;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mynamebattler.Constant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
/*
　データベースの作成・更新を行うクラス
 */
public class DBOpenHelper extends SQLiteOpenHelper implements Constant {

    //データベースの情報
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyNameBattler.db";

    private String SQL = "CREATE TABLE PA_TBL(_pa_id INTEGER PRIMARY KEY,pa_name TEXT NOT NULL,pa_con TEXT NOT NULL,pa_eff TEXT NOT NULL)";

    Context mContext;

    DBOpenHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        mContext = context;
    }

    //DB作成
    @Override
    public void onCreate(SQLiteDatabase db){
        //db.execSQL(SQL);

        try{
            execSql(db,"sql_file/create");
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    //DBアップグレード
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion){
        //既存のテーブルを削除
        try{
            execSql(db,"sql_file/delete");
        } catch (IOException e){
            e.printStackTrace();
        }
        //DB再作成
        onCreate(db);
    }

    //DBダウングレード
    public void onDowngrade(SQLiteDatabase db,int oldVersion,int newVersion){
        onUpgrade(db,oldVersion,newVersion);
    }

    //assetsのSQL文を呼び出して実行する
    private void execSql(SQLiteDatabase db,String assetsDir) throws IOException{
        AssetManager as = mContext.getResources().getAssets();
        try{
            String files[] = as.list(assetsDir);
            for(int i =0;i < files.length;i++){
                String str =readFile(as.open(assetsDir + "/" + files[i]));
                for(String sql: str.split("/")){
                    db.execSQL(sql);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    //ファイルから文字列を読み込む
    private String readFile(InputStream is) throws IOException{
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(is,"UTF-8"));

            StringBuilder sb = new StringBuilder();
            String str;
            while ((str = br.readLine()) != null){
                sb.append(str + "\n");
            }
            return sb.toString();
        } finally {
            if(br != null) br.close();
        }
    }
}
