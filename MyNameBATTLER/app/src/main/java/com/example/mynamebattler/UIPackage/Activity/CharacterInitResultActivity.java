package com.example.mynamebattler.UIPackage.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.JobManager;
import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.PlayerPackage.PlayerData;
import com.example.mynamebattler.R;
import com.example.mynamebattler.SkillPackage.Skill;
import com.example.mynamebattler.UIPackage.Fragment.CharacterInfoFragment;

import java.util.ArrayList;
/*
　キャラクター作成完了画面の表示及びUI処理を行うクラス
 */
public class CharacterInitResultActivity extends AppCompatActivity implements Constant {

    private String characterName,jobName;
    private int hp,str,def,agi,dex,luck,PAID,jobID;

    private ArrayList<String> charaInfo,jobInfo,PAInfo;

    private CharacterDataManager characterDataManager;
    private JobManager jobManager;
    private PAManager paManager;
    private SkillManager skillManager;
    private SoundManager soundManager;
    private ImageManager imageManager;

    private Bundle bundle;
    private CharacterInfoFragment characterInfoFragment;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_init_result_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスの取得
        characterDataManager = CharacterDataManager.getInstance();
        jobManager = JobManager.getInstance();
        paManager = PAManager.getInstance();
        skillManager = SkillManager.getInstance();
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();

        //インテントから各種データを取得
        GetIntentData();

        //キャラクターのステータスを取得
        GetCharaStatus();

        //各UIの表示＆設定
        InitCharacterInfoFragment();
        InitButtonUI();
    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //Intentからデータを取得
    private void GetIntentData(){
        //Intentから各種データを取得
        Intent intent = getIntent();
        characterName = intent.getStringExtra(CHARACTER_NAME_KEY); //キャラクター名の取得
        jobID = intent.getIntExtra(JOB_ID_KEY,0); //職業IDの取得
        jobName = intent.getStringExtra(JOB_NAME_KEY); //職業名の取得
        PAID = intent.getIntExtra(PA_ID_KEY,0); //PAIDの取得

    }

    //キャラクターステータスを作成して保存
    private void GetCharaStatus(){
        PlayerData playerDataDataCharacter = characterDataManager.CharacterDataCreate(characterName,jobID);

        //作成されたステータスを格納
        hp = playerDataDataCharacter.getDefaultHp();
        str = playerDataDataCharacter.getDefaultStr();
        def = playerDataDataCharacter.getDefaultDef();
        agi = playerDataDataCharacter.getDefaultAgi();
        dex = playerDataDataCharacter.getDefaultDex();
        luck = playerDataDataCharacter.getDefaultLuck();
        PAInfo = paManager.GetPAInfo(PAID);

        DBSaveCharacterData(); //作成したキャラクターをDBへ保存
    }

    //テキストの設定＆処理
    private void InitCharacterInfoFragment(){

        //キャラクター情報の取得
        int charaId = characterDataManager.GetCharacterID(characterName);
        charaInfo = characterDataManager.GetCharacterInfo(charaId);
        bundle.putStringArrayList(CHARACTER_INFO_LIST_KEY,charaInfo);

        //職業情報の取得
        jobInfo = jobManager.GetJobInfo(Integer.valueOf(charaInfo.get(2)));
        bundle.putStringArrayList(JOB_INFO_LIST_KEY,jobInfo);

        //スキル情報の取得
        ArrayList<Skill> allSkill = new ArrayList<Skill>();
        allSkill = skillManager.GetAllSkill(jobInfo.get(7));

        allSkillId = new ArrayList<String>();
        allSkillName = new ArrayList<String>();
        allSkillCost = new ArrayList<Integer>();
        allSkillEff = new ArrayList<String>();

        for(Skill skill:allSkill){
            allSkillId.add(skill.getSkillId());
            allSkillName.add(skill.getSkillName());
            allSkillCost.add(skill.getSkillCost());
            allSkillEff.add(skill.getSkillEff());
        }

        //Bundleに選択している職業のスキル一覧を格納
        bundle.putStringArrayList(SKILL_ID_LIST_KEY,allSkillId);
        bundle.putStringArrayList(SKILL_NAME_LIST_KEY,allSkillName);
        bundle.putIntegerArrayList(SKILL_COST_LIST_KEY,allSkillCost);
        bundle.putStringArrayList(SKILL_EFF_LIST_KEY,allSkillEff);

        //PA情報の取得
        PAInfo = paManager.GetPAInfo(Integer.valueOf(charaInfo.get(9)));
        bundle.putStringArrayList(PA_INFO_LIST_KEY,PAInfo);

        //characterInfoFragmentを作成して表示
        characterInfoFragment = new CharacterInfoFragment();
        characterInfoFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.initCharacterInfoFlame,characterInfoFragment);
        transaction.commit();

    }

    //ボタンの設定＆処理
    private void InitButtonUI(){
        //キャラクター作成画面へ遷移
        ImageButton createAgainBtn = findViewById(R.id.SendCharInitActButton);
        Bitmap btnImage1 = imageManager.GetImageBitmap(this,Constant.CREATE_AGAIN_BUTTON_IMAGE);
        createAgainBtn.setImageBitmap(btnImage1);
        createAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),CharacterInitActivity.class);
                startActivity(intent);
            }
        });

        //キャラクター一覧画面へ遷移
        ImageButton characterListBtn = findViewById(R.id.SendCharListActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.CHARACTER_LIST_BUTTON_IMAGE);
        characterListBtn.setImageBitmap(btnImage2);
        characterListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),CharacterListActivity.class);
                startActivity(intent);
            }
        });

        //タイトル画面へ遷移
        ImageButton backTitleBtn = findViewById(R.id.SendTitleActButton);
        Bitmap btnImage3 = imageManager.GetImageBitmap(this,BACK_TITLE_BUTTON_IMAGE);
        backTitleBtn.setImageBitmap(btnImage3);
        backTitleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),TitleActivity.class);
                startActivity(intent);
            }
        });
    }

    //キャラクター情報をDBに保存する
    private void DBSaveCharacterData(){
        characterDataManager.InsertCharacterData(characterName,jobID,hp,str,def,agi,dex,luck,PAID);
    }
}
