package com.example.mynamebattler.UIPackage.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.BattleManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.PartyManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Fragment.EnemyPartyFragment;
import com.example.mynamebattler.UIPackage.Fragment.MyPartyFragment;
/*
　バトル結果画面の表示及びUI処理を行うクラス
 */
public class BattleResultActivity extends AppCompatActivity implements Constant {

    private PartyManager partyManager;
    private BattleManager battleManager;
    private Bundle bundle;
    private MyPartyFragment myPartyFragment;
    private EnemyPartyFragment enemyPartyFragment;
    private ImageManager imageManager;
    private SoundManager soundManager;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battle_result_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスを取得
        partyManager = PartyManager.getInstance();
        battleManager = BattleManager.getInstance();
        imageManager = ImageManager.getInstance();
        soundManager = SoundManager.getInstance();

        //リザルトBGMの再生
        soundManager.ResultBGMStart(this);

        InitMyPartyFragment();
        InitEnemyPartyFragment();
        InitImageView();
        InitButtonUI();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //MyPartyFragmentの設定
    private void InitMyPartyFragment(){
        myPartyFragment = new MyPartyFragment();

        //Bundleへパーティー情報を格納
        bundle.putSerializable(MYPARTY_PLAYER1_KEY,partyManager.getMyParty().get(0));
        bundle.putSerializable(MYPARTY_PLAYER2_KEY,partyManager.getMyParty().get(1));
        bundle.putSerializable(MYPARTY_PLAYER3_KEY,partyManager.getMyParty().get(2));

        //MyPartyFragmentにパーティーを送り、更新
        myPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mypartyFlame,myPartyFragment);
        transaction.commit();

    }

    //EnemyPartyFragmentの設定
    private void InitEnemyPartyFragment(){

        enemyPartyFragment = new EnemyPartyFragment();

        //Bundleへパーティー情報を格納
        bundle.putSerializable(ENEMYPARTY_PLAYER1_KEY,partyManager.getEnemyParty().get(0));
        bundle.putSerializable(ENEMYPARTY_PLAYER2_KEY,partyManager.getEnemyParty().get(1));
        bundle.putSerializable(ENEMYPARTY_PLAYER3_KEY,partyManager.getEnemyParty().get(2));

        //MyPartyFragmentにパーティーを送り、更新
        enemyPartyFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.enemypartyFlame,enemyPartyFragment);
        transaction.commit();
    }

    //画像の設定＆表示
    private void InitImageView(){
        ImageView battleResultImageView = findViewById(R.id.BattleResultImageView);
        Bitmap image = imageManager.GetBattleResultImage(this, battleManager.isMyPartyWin());
        battleResultImageView.setImageBitmap(image);

    }

    //ボタンの設定＆処理
    private void InitButtonUI(){

        //タイトル画面へ遷移
        ImageButton backTitleBtn = findViewById(R.id.SendTitleActButton);
        Bitmap btnImage1 = imageManager.GetImageBitmap(this,Constant.BACK_TITLE_BUTTON_IMAGE);
        backTitleBtn.setImageBitmap(btnImage1);
        backTitleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(),TitleActivity.class);
                startActivity(intent);
            }
        });

        //バトル画面へ遷移(再戦)
        ImageButton retryBattleBtn = findViewById(R.id.SendBattleSceneActButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.RETRY_BATTLE_BUTTON_IMAGE);
        retryBattleBtn.setImageBitmap(btnImage2);
        retryBattleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //確認ダイアログを表示
                new AlertDialog.Builder(BattleResultActivity.this)
                        .setTitle("RETRY BATTLE")
                        .setMessage("同じ対戦相手と再戦しますか？")
                        .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //はいを押下で同じ対戦相手と対戦
                                partyManager.ResetAllPartyMember();
                                Intent intent = new Intent(getApplication(),BattleSceneActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("いいえ", null)
                        .show();
            }
        });

        //パーティー編成画面へ遷移
        ImageButton nextBattleBtn = findViewById(R.id.SendBattleReadyActButton);
        Bitmap btnImage3 = imageManager.GetImageBitmap(this,Constant.NEXT_BATTLE_BUTTON_IMAGE);
        nextBattleBtn.setImageBitmap(btnImage3);
        nextBattleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //確認ダイアログを表示
                new AlertDialog.Builder(BattleResultActivity.this)
                        .setTitle("NEXT BATTLE")
                        .setMessage("引き続きバトルを続けますか？")
                        .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //はいを押下で同じパーティー編成画面へ遷移
                                Intent intent = new Intent(getApplication(),PartyFormationActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("いいえ", null)
                        .show();
            }
        });
    }

}
