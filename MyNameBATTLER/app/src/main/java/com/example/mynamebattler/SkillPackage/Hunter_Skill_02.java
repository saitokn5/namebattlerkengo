package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ハンター - スキル2
　スモークショット
 */
public class Hunter_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog("相手の視界を奪う一撃を放った！\n");

            //STRを0.8倍
            double str = actionMember.getBattleStr() * 0.8;
            actionMember.setBattleStr((int) str);

            //攻撃
            actionMember.Attack(actionMember,target);

            //暗闇を付与
            target.setStateBlind(true);
            actionMember.setActLog(target.getPlayerName() + "は暗闇になった！\n");

            //STRを元に戻す
            actionMember.setBattleStr(actionMember.getDefaultStr());
        }
    }
}
