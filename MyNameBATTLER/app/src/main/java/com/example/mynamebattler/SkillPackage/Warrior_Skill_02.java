package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　ウォーリアー - スキル2
　ウォークライ
 */
public class Warrior_Skill_02 extends Skill implements Serializable {

    @Override
    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){
        actionMember.setActLog(actionMember.getPlayerName() + "は" + skillName + "を使用した！\n");
        if(myParty.getSp() < skillCost){
            //SPが足りない場合
            actionMember.setActLog("しかしSPが足らなかった…\n\n");
        } else {
            //スキルを使用した場合
            myParty.SPCountUpdate(-skillCost);
            actionMember.setActLog(target.getPlayerName() + "は叫び戦意を高揚させた！\n");

            //最大HPの30％分を消費(現HPが消費HP未満の場合は1残すようにする)
            double usedHp = actionMember.getDefaultHp() * 0.3;
            if(actionMember.getBattleHp() < usedHp){
                usedHp = actionMember.getBattleHp() - 1;
            }
            actionMember.setBattleHp(actionMember.getBattleHp() - (int)usedHp);

            //SPを増やす
            myParty.SPCountUpdate(50);
            actionMember.setActLog(actionMember.getPlayerName() + "のHP" + usedHp + "を消費し、自身のパーティーSPを50増加！\n\n");

        }
    }
}
