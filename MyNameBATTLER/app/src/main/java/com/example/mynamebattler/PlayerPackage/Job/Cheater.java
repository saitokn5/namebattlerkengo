package com.example.mynamebattler.PlayerPackage.Job;

import com.example.mynamebattler.Constant;
import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.Calculation;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
職業：転生者(Playerクラスを継承)
 */
public class Cheater extends PlayerData implements Constant, Serializable {

    public Cheater(String name){
        super(name);
    }

    //名前から取得したステータスでCharacter作成
    @Override
    protected void CharaCreate(){
        int randomIndex = (int)(Math.random()*10);
        defaultHp = (GetNumber(randomIndex,STATUS_RANGE_BASE) + STATUS_RANGE_C) * STATUS_HP_FACTOR;
        defaultStr = GetNumber(randomIndex+1,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultDef = GetNumber(randomIndex+2,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultAgi = GetNumber(randomIndex+3,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultDex = GetNumber(randomIndex+4,STATUS_RANGE_BASE) + STATUS_RANGE_D;
        defaultLuck = GetNumber(randomIndex+5,STATUS_RANGE_BASE) + STATUS_RANGE_D;
    }

    //攻撃用メソッド
    @Override
    public void Attack(PlayerData actionMember, PlayerData target){
        actLog.append(playerName + "の攻撃！\n");
        double damage = Calculation.DamageCalculation(this,target);
        if(JACheck){
            damage = damage * 1.2;
        }
        target.HitDamage((int)damage);
        actLog.append(target.getPlayerName() + "に" + damage +"のダメージ\n\n");
        if(attackPoison){
            target.setStatePoison(true);
            actLog.append(target.getPlayerName() + "は毒になった。\n");
        }
    }

    //JA用メソッド
    @Override
    public void JA(PlayerData actionMember, Party myParty, Party enemyParty){
        //自パーティーのSP100以上の場合、発動
        if(myParty.getSp() >= 100){
            setJACheck(true);
            actLog.append(getPlayerName() + "のJA発動！\n最終与ダメージがアップ！\n\n");
        } else {
            setJACheck(false);
        }
    }

}
