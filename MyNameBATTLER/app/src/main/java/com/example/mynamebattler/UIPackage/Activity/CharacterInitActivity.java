package com.example.mynamebattler.UIPackage.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mynamebattler.DBPackage.DBManager;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.ManagerPackage.CharacterDataManager;
import com.example.mynamebattler.ManagerPackage.ImageManager;
import com.example.mynamebattler.ManagerPackage.JobManager;
import com.example.mynamebattler.ManagerPackage.PAManager;
import com.example.mynamebattler.ManagerPackage.SkillManager;
import com.example.mynamebattler.ManagerPackage.SoundManager;
import com.example.mynamebattler.R;
import com.example.mynamebattler.SkillPackage.Skill;
import com.example.mynamebattler.UIPackage.Fragment.HeaderFragment;
import com.example.mynamebattler.UIPackage.Fragment.SkillListDialogFragment;

import java.util.ArrayList;
/*
　キャラクター作成画面の表示及びUI処理を行うクラス
 */
public class CharacterInitActivity extends AppCompatActivity implements Constant {

    private String selectedJobName, selectedPAName;
    private int selectedJobID,selectedPAID;

    private Bundle bundle;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;

    private JobManager jobManager;
    private SkillManager skillManager;
    private PAManager paManager;
    private SoundManager soundManager;
    private ImageManager imageManager;
    private CharacterDataManager characterDataManager;
    private HeaderFragment headerFragment;

    private Spinner jobSpinner,paSpinner;

    //端末の戻るを禁止する
    @Override
    public void onBackPressed(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_init_activity);

        //Bundleの初期化
        bundle = new Bundle();

        //各種インスタンスの取得
        jobManager = JobManager.getInstance();
        skillManager = SkillManager.getInstance();
        paManager = PAManager.getInstance();
        soundManager = SoundManager.getInstance();
        imageManager = ImageManager.getInstance();
        characterDataManager = CharacterDataManager.getInstance();

        InitSpinnerUI();
        InitHeaderFragment();
        InitButtonUI();

    }

    //Activityが隠れた時に呼び出し
    @Override
    protected void onPause(){
        super.onPause();
        soundManager.BGMPause();
    }

    //Activityが前面に表示された時に呼び出し
    protected void onResume(){
        super.onResume();
        //BGMの再開
        soundManager.BGMReStart();
    }

    //Activityが破棄されるタイミングでサービスを終了させる
    @Override
    protected void onDestroy(){
        super.onDestroy();
        soundManager.BGMStop();
    }

    //スピナーの設定
    private void InitSpinnerUI(){
        //ジョブスピナーの設定
        jobSpinner = (Spinner)findViewById(R.id.JobSpinner);
        jobSpinner.setAdapter(jobManager.GetJobSpinnerItem(this));

        //ジョブを選択した時の挙動
        jobSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //ジョブが選択された時に、選択した職業の情報をテキストへ反映
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedJobName = jobSpinner.getSelectedItem().toString();
                selectedJobID = jobManager.GetJobID(selectedJobName); //職業名からJobID取得

                ArrayList<String> jobInfo = new ArrayList<String>();
                jobInfo = jobManager.GetJobInfo(selectedJobID);

                TextView hpRange = findViewById(R.id.HPRangeText);
                TextView strRange = findViewById(R.id.StrRangeText);
                TextView defRange = findViewById(R.id.DefRangeText);
                TextView agiRange = findViewById(R.id.AgiRangeText);
                TextView dexRange = findViewById(R.id.DexRangeText);
                TextView luckRange = findViewById(R.id.LuckRangeText);
                TextView JAText = findViewById(R.id.JAText);
                TextView jobText = findViewById(R.id.JobText);

                hpRange.setText(jobInfo.get(1));
                strRange.setText(jobInfo.get(2));
                defRange.setText(jobInfo.get(3));
                agiRange.setText(jobInfo.get(4));
                dexRange.setText(jobInfo.get(5));
                luckRange.setText(jobInfo.get(6));

                ArrayList<Skill> allSkill = new ArrayList<Skill>();
                allSkill = skillManager.GetAllSkill(jobInfo.get(7));

                allSkillId = new ArrayList<String>();
                allSkillName = new ArrayList<String>();
                allSkillCost = new ArrayList<Integer>();
                allSkillEff = new ArrayList<String>();

                for(Skill skill:allSkill){
                    allSkillId.add(skill.getSkillId());
                    allSkillName.add(skill.getSkillName());
                    allSkillCost.add(skill.getSkillCost());
                    allSkillEff.add(skill.getSkillEff());
                }

                JAText.setText(jobInfo.get(8));
                jobText.setText(jobInfo.get(9));
            }

            //職業が選択されていない時(今回は使用しない)
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //PAスピナーの設定
        paSpinner = (Spinner)findViewById(R.id.PASpinner);
        paSpinner.setAdapter(paManager.GetPASpinnerItem(this));

        //PAを選択した時の挙動
        paSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //PAが選択された時、選択したPAの情報を取得
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPAName = paSpinner.getSelectedItem().toString(); //選択したPAを取得
                selectedPAID = paManager.GetPAID(selectedPAName); //PA名からPAIDを取得

                TextView PAText = findViewById(R.id.PAText);
                ArrayList<String> paInfo = new ArrayList<String>();

                paInfo = paManager.GetPAInfo(selectedPAID);
                PAText.setText(paInfo.get(1) + "\n" + paInfo.get(2));

            }
            //PAが選択されていない時(今回は使用しない)
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //HeaderFragmentの表示
    private void InitHeaderFragment(){

        bundle.putString(BACK_ACT_CHECK_KEY,BACK_CHARACTER_LIST_ACT_KEY);
        headerFragment = new HeaderFragment();
        headerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.headerFragmentFrame,headerFragment);
        transaction.commit();
    }

    //ボタンの設定&処理
    private void InitButtonUI(){

        //スキル一覧ボタンを押下時の動作
        Button skillListButton = findViewById(R.id.SkillListButton);
        skillListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Bundleに選択している職業のスキル一覧を格納
                bundle.putStringArrayList(SKILL_ID_LIST_KEY,allSkillId);
                bundle.putStringArrayList(SKILL_NAME_LIST_KEY,allSkillName);
                bundle.putIntegerArrayList(SKILL_COST_LIST_KEY,allSkillCost);
                bundle.putStringArrayList(SKILL_EFF_LIST_KEY,allSkillEff);

                //skillListDialogFragmentを表示
                DialogFragment skillListDialogFragment = new SkillListDialogFragment();
                skillListDialogFragment.setArguments(bundle);
                skillListDialogFragment.show(getSupportFragmentManager(),"skill_list_dialog");
            }
        });

        //createボタンを押下
        ImageButton createBtn = findViewById(R.id.CreateButton);
        Bitmap btnImage2 = imageManager.GetImageBitmap(this,Constant.CREATE_BUTTON_IMAGE);
        createBtn.setImageBitmap(btnImage2);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GetInitCharacterName().length() == 0 || GetInitCharacterName().trim().equals("")){
                    //キャラクター名が未入力(空白のみの入力含む)の場合はエラー
                    new AlertDialog.Builder(CharacterInitActivity.this)
                            .setTitle("エラー")
                            .setMessage("キャラクター名を入力してください！")
                            .setNegativeButton("OK",null)
                            .show();
                } else if(characterDataManager.CharacterNameDuplicationCheck(GetInitCharacterName())){
                    //入力した名前が既に登録されていないかチェックし、重複が無ければキャラクター作成
                    //Intentの設定(入力・選択したキャラクター情報を格納)
                    Intent intent = new Intent(getApplication(), CharacterInitResultActivity.class);
                    intent.putExtra(CHARACTER_NAME_KEY, GetInitCharacterName());
                    intent.putExtra(JOB_ID_KEY, selectedJobID);
                    intent.putExtra(JOB_NAME_KEY, selectedJobName);
                    intent.putExtra(PA_ID_KEY, selectedPAID);

                    //画面遷移
                    startActivity(intent);
                } else {
                    //キャラクター名が既に登録済みの場合エラー
                    new AlertDialog.Builder(CharacterInitActivity.this)
                            .setTitle("エラー")
                            .setMessage("既に登録されているキャラクター名です！")
                            .setNegativeButton("OK",null)
                            .show();
                }
            }
        });
    }

    //入力された名前を取得
    private String GetInitCharacterName(){
        //名前の入力
        final EditText initNameTextBox = findViewById(R.id.InitNameTextBox);
        String charaName = initNameTextBox.getText().toString();
        return charaName;
    }

}
