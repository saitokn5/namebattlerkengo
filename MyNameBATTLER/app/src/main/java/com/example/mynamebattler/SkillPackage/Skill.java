package com.example.mynamebattler.SkillPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;

import java.io.Serializable;

/*
　スキル用の基底クラス
 */
public class Skill implements Serializable {

    protected String skillId;
    protected int skillCost;
    protected String skillName;
    protected String skillEff;

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public int getSkillCost() {
        return skillCost;
    }

    public void setSkillCost(int skillCost) {
        this.skillCost = skillCost;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public String getSkillEff() {
        return skillEff;
    }

    public void setSkillEff(String skillEff) {
        this.skillEff = skillEff;
    }

    public void Invoke(PlayerData target, PlayerData actionMember, Party myParty, Party enemyParty){

    }

}
