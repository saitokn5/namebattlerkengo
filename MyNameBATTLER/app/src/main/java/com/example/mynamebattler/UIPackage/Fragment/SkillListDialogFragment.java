package com.example.mynamebattler.UIPackage.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.example.mynamebattler.AdapterPackage.SkillListViewAdapter;
import com.example.mynamebattler.Constant;
import com.example.mynamebattler.R;
import com.example.mynamebattler.UIPackage.Activity.CharacterInfoActivity;

import java.util.ArrayList;
/*
　スキル一覧ダイアログを管理するクラス
 */
public class SkillListDialogFragment extends DialogFragment implements Constant {

    private View view;

    private Bundle bundle;
    private ArrayList<String> allSkillId, allSkillName, allSkillEff;
    private ArrayList<Integer> allSkillCost;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        //バンドルから各種データを取得
        bundle = getArguments();
        allSkillId = bundle.getStringArrayList(SKILL_ID_LIST_KEY);
        allSkillName = bundle.getStringArrayList(SKILL_NAME_LIST_KEY);
        allSkillCost = bundle.getIntegerArrayList(SKILL_COST_LIST_KEY);
        allSkillEff = bundle.getStringArrayList(SKILL_EFF_LIST_KEY);

        //ダイアログビルダーを作成
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //viewの取得
        view = LayoutInflater.from(getActivity()).inflate(R.layout.skill_list_fragment,null);

        InitSkillList();

        builder.setView(view)
                .setCustomTitle(GetCustomTitle(view))
                .setNegativeButton("閉じる",null);

        return builder.create();
    }

    //スキル一覧を作成する
    private void InitSkillList(){
        ListView skillList = view.findViewById(R.id.skillList);
        BaseAdapter adapter = new SkillListViewAdapter(view.getContext(),R.layout.skill_list_item,
                allSkillId,
                allSkillName,
                allSkillCost,
                allSkillEff);
        skillList.setAdapter(adapter);
    }

    //ダイアログのタイトルを作成
    private TextView GetCustomTitle(View view){
        TextView customTitle = new TextView(view.getContext());
        customTitle.setText(R.string.SkillListTile); //表示テキスト
        customTitle.setGravity(Gravity.CENTER_HORIZONTAL); //中央揃え
        customTitle.setTextSize(24);
        customTitle.setTypeface(Typeface.DEFAULT_BOLD); //太字

        return customTitle;
    }
}
