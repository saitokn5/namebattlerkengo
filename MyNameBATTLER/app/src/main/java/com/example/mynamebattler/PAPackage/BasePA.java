package com.example.mynamebattler.PAPackage;

import com.example.mynamebattler.PartyPackage.Party;
import com.example.mynamebattler.PlayerPackage.PlayerData;
/*
　PAクラスのインターフェース
 */
public interface BasePA {

    void Invoke(PlayerData actionMember, Party myParty, Party enemyParty);

}
