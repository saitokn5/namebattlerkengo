CREATE TABLE ENEMY_TBL
(
_enemy_id INTEGER PRIMARY KEY,
enemy_name TEXT NOT NULL,
_job_id INTEGER NOT NULL,
hp INTEGER NOT NULL,
str INTEGER NOT NULL,
def INTEGER NOT NULL,
agi INTEGER NOT NULL,
dex INTEGER NOT NULL,
luck INTEGER NOT NULL,
_pa_id INTEGER NOT NULL
)
/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1001,'ドリアール',101,2350,231,220,177,156,57,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1002,'アーミュー',101,2240,201,225,193,169,61,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1003,'ジャスカー',101,2360,237,240,155,174,74,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1004,'トバイモン',101,2210,200,247,161,152,93,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1005,'ジャイシー',101,2260,245,207,183,196,53,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1006,'ベネテリー',101,2310,233,223,181,193,61,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1007,'ゲイブラッド',101,2390,231,221,192,172,92,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1008,'デーヴィッド',101,2000,224,209,153,187,57,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1009,'ニコラリー',101,2260,220,201,176,184,67,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1010,'ジョナンド',101,2450,245,245,192,177,82,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1011,'パトリック',101,2020,233,229,196,198,89,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1012,'ルフレット',101,2150,224,244,163,154,77,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1013,'クスタント',101,2200,243,249,190,181,67,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1014,'ホレス',101,2320,233,220,177,194,84,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1015,'フェビアン',101,2080,249,224,188,171,66,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1016,'アーローム',102,1020,153,127,224,173,201,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1017,'ヴァレッド',102,1450,196,128,231,156,240,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1018,'ルドウエン',102,1310,192,107,229,177,209,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1019,'エセルイス',102,1090,179,108,241,164,205,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1020,'コーニエル',102,1120,185,135,207,194,200,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1021,'モイモレク',102,1220,162,112,231,196,209,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1022,'ルコシエル',102,1400,155,132,239,173,228,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1023,'ルーレプト',102,1360,189,138,248,178,234,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1024,'ラシアダド',102,1180,158,143,216,184,227,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1025,'ベザル',102,1000,191,133,235,174,207,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1026,'アメルカス',102,1210,187,106,219,194,236,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1027,'アムニエン',102,1440,172,144,246,194,243,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1028,'オロバリル',102,1080,195,133,223,158,201,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1029,'ウァサゴー',102,1360,187,112,216,189,218,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1030,'ベリアモン',102,1010,152,131,200,190,210,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1031,'エギビゴル',103,1020,213,177,108,65,280,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1032,'アドラース',103,1380,234,177,138,91,252,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1033,'フィステマ',103,1180,246,188,129,82,281,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1034,'ダンタムズ',103,1490,213,168,111,68,254,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1035,'ウリクサス',103,1320,231,175,123,71,299,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1036,'ベルファス',103,1200,237,152,108,97,256,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1037,'リバイモン',103,1300,205,167,123,54,277,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1038,'ウェパズズ',103,1230,219,168,119,90,271,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1039,'アグナック',103,1200,237,187,130,84,263,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1040,'セエレ',103,1320,219,171,140,89,285,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1041,'ダイアニー',103,1320,214,157,118,78,291,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1042,'シャローナ',103,1110,248,155,118,97,259,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1043,'ドライーズ',103,1200,228,188,105,69,292,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1044,'リンジャー',103,1160,243,188,111,71,290,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1045,'カーラ',103,1440,224,184,124,97,294,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1046,'リザベティ',104,2310,176,269,76,249,105,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1047,'ケイ',104,2460,189,252,92,246,134,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1048,'アントニア',104,2090,180,253,85,227,128,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1049,'ブリジェマ',104,2440,192,252,53,225,104,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1050,'キャエレン',104,2390,168,279,61,241,112,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1051,'ローレイン',104,2090,187,286,73,203,124,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1052,'ジョセアラ',104,2460,196,255,93,215,115,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1053,'ディアリー',104,2310,166,257,90,235,133,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1054,'コール',104,2430,171,259,83,238,121,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1055,'エルヴィラ',104,2170,194,268,90,220,117,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1056,'アトリエット',105,1360,166,75,275,282,184,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1057,'アイヴィス',105,1330,169,52,299,251,184,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1058,'ヴィヴェラ',105,1050,180,92,265,286,172,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1059,'クラリーナ',105,1360,198,78,259,257,184,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1060,'ダーリジット',105,1240,192,66,287,262,169,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1061,'ヴェランコ',105,1420,183,85,276,286,183,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1062,'ヴェネデット',105,1390,172,90,274,299,168,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1063,'エラルタコ',105,1320,187,52,288,294,155,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1064,'ターヴィオ',105,1020,166,64,294,296,179,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1065,'アンニコラ',105,1120,180,82,268,292,177,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1066,'ベネディオ',106,2880,283,108,139,92,87,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1067,'ティアーノ',106,2640,278,121,148,97,93,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1068,'サラディオ',106,2580,299,110,128,94,66,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1069,'ルフレート',106,2900,289,145,127,77,87,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1070,'ルメネーア',106,2520,283,113,122,65,65,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1071,'アンセスト',106,2850,267,126,119,95,70,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1072,'アポリスタ',106,2630,263,106,118,74,62,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1073,'ミントーレ',106,2520,262,120,106,64,67,203)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1074,'アンセルモ',106,2940,260,129,141,71,94,204)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1075,'バルダンテ',106,2970,258,143,145,83,54,205)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1076,'アナスパレ',107,1890,105,116,132,112,149,206)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1077,'ルクレンゾ',107,1740,143,126,108,101,129,207)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1078,'ジルベルト',107,1810,103,124,139,120,108,201)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1079,'ヴァレーモ',107,1950,148,112,145,124,126,202)/
INSERT INTO ENEMY_TBL(_enemy_id,enemy_name,_job_id,hp,str,def,agi,dex,luck,_pa_id)values(1080,'ファエーレ',107,1800,119,137,141,139,142,203)
