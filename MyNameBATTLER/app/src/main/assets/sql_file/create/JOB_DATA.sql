﻿CREATE TABLE JOB_TBL
(
_job_id INTEGER PRIMARY KEY,
job_name TEXT NOT NULL,
hp_range TEXT NOT NULL,
str_range TEXT NOT NULL,
def_range TEXT NOT NULL,
agi_range TEXT NOT NULL,
dex_range TEXT NOT NULL,
luck_range TEXT NOT NULL,
skill_group_id TEXT NOT NULL,
ja TEXT NOT NULL,
job_info TEXT NOT NULL
)
/
INSERT INTO JOB_TBL
(_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
values
(101,
"ファイター",
"B",
"B",
"B",
"C",
"C",
"E",
"FIG",
"【食いしばり】戦闘不能時に一度だけ最大HPの10％で復活する",
"バランスの良いアタッカー。堅実な戦いが可能。"
)
/
INSERT INTO JOB_TBL
 (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
 values
 (102,
 "ウィザード",
 "D",
 "C",
 "D",
 "B",
 "C",
 "B",
 "WIZ",
 "【魔法障壁】〈自パーティーのSP40以上〉全ての被ダメージを25％抑える",
 "スキルが強力なアタッカー。SPへの依存度が高く、SPが貯まる前の打たれ弱さが弱点。"
 )
 /
 INSERT INTO JOB_TBL
 (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
 values
 (103,
 "クレリック",
 "D",
 "B",
 "C",
 "D",
 "E",
 "A",
 "CLR",
 "【神の加護】〈自パーティーのSP60以上〉スキル使用時にスキルの効果を味方全体にする",
 "一般的なヒーラー。攻撃力もそこそこの為、攻撃も可能。"
 )
 /
  INSERT INTO JOB_TBL
  (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
  values
  (104,
  "ナイト",
  "B",
  "C",
  "A",
  "E",
  "B",
  "D",
  "KNI",
  "【タンクの心得】〈自身のHP80％以上〉毎ターン自動でSPが5ずつ増加",
  "最高のDEFと高ランクのHPが特徴。他の能力は平均以下の為、SPを貯めてパーティー全体の行動をサポートするのが役目"
  )
  /
   INSERT INTO JOB_TBL
   (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
   values
   (105,
   "ハンター",
   "D",
   "C",
   "E",
   "A",
   "A",
   "C",
   "HUN",
   "【集中力】〈自パーティーのSP70以上〉自身のDEXが50％アップする",
   "そこそこの攻撃力と最高のAGL＆DEX、全体攻撃スキルで確実にダメージを与えていくアタッカー。非常に打たれ弱い。"
   )
   /
      INSERT INTO JOB_TBL
      (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
      values
      (106,
      "ウォーリアー",
      "A",
      "A",
      "D",
      "D",
      "E",
      "E",
      "WAR",
      "【狂戦士】〈自身のHP10％未満〉自身のSTRが50％アップする",
      "一発の攻撃力に特化したアタッカー。強力なデメリット付きスキルをどう生かすかがカギ。"
      )
      /
            INSERT INTO JOB_TBL
            (_job_id,job_name,hp_range,str_range,def_range,agi_range,dex_range,luck_range,skill_group_id,ja,job_info)
            values
            (107,
            "転生者",
            "C",
            "D",
            "D",
            "D",
            "D",
            "D",
            "CHE",
            "【未知の力】〈自パーティーのSP100以上〉攻撃時の最終ダメージを20％アップ",
            "基本的な能力は低いが、発動できれば強力なスキルとJAが特徴。"
            )
